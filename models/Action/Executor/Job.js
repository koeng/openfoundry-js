'use strict'

const mongoose = require('mongoose');

const modelSchema = mongoose.Schema({
  requestedAt    : { type: String, default: new Date() },
  updatedAt    : { type: String, default: new Date() },
  startedAt : { type: String, default: new Date() },
  completedAt: { type: String, default: new Date() },
  failedAt: { type: String, default: new Date() },

  parentProcessId: String,
  name         : { type: String, required: true },
  description  : { type: String, required: true },
  job_status   : { type: String, default: "Submitted" },
  job_origin   : { type: String, required: true }, // Bionet or OpenFoundry
  depends_on   : { type: Array, required: true }, // Can only execute after these jobs

  requesterName: { type: String, required: true },
  requesterEmail: { type: String, required: true },

  operatorName: { type: String, required: true },
  operatorEmail: { type: String, required: true },
  
  // Tasks point to jobs

});

module.exports = mongoose.model('ExecutorJob', modelSchema);

// Possible status:
//
// Submitted, Rejected, In-Progress, Completed, Failed
