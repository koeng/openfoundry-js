'use strict'

const mongoose = require('mongoose');

const modelSchema = mongoose.Schema({
  createdAt    : { type: String, default: new Date() },
  startedAt    : { type: String, default: new Date() },
  completedAt  : { type: String, default: new Date() },
  parentJobId  : { type: String, required: true},
  target : { type: Array, required: true}, // Array of targets

  to : { type: Array, required: true}, // Array of wells to transfer to
  volume: {type: Number, required: true},

  volume_type: { type: String, required: true}, // Liquid, Vicious, Transformants
});

module.exports = mongoose.model('TaskTransfer', modelSchema);

// Array of targets and to are figured out AND checked earlier. 
//
// The target and to MUST be in a container you can execute 'Transfer' in
