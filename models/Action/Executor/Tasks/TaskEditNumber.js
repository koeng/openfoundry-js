'use strict'

const mongoose = require('mongoose');

const modelSchema = mongoose.Schema({
  createdAt    : { type: String, default: new Date() },
  startedAt    : { type: String, default: new Date() },
  completedAt  : { type: String, default: new Date() },
  parentJobId  : { type: String, required: true},
  target : { type: String, required: true},

  attr : { type: String, required: true},
  add : { type: Number, required: true}, // Use negative number to subtract
});

module.exports = mongoose.model('TaskEditNumber', modelSchema);

