'use strict'

const mongoose = require('mongoose');

const modelSchema = mongoose.Schema({
  createdAt    : { type: String, default: new Date() },
  startedAt    : { type: String, default: new Date() },
  completedAt  : { type: String, default: new Date() },
  parentJobId  : { type: String, required: true},

  time : { type: Number, required: true},

});

module.exports = mongoose.model('TaskWait', modelSchema);

