'use strict'

const mongoose = require('mongoose');

const modelSchema = mongoose.Schema({
  createdAt    : { type: String, default: new Date() },
  startedAt    : { type: String, default: new Date() },
  completedAt  : { type: String, default: new Date() },
  parentJobId  : { type: String, required: true},

  parentId : { type: String, required: true},
  object: { type: String, required: true}, //json format required for specific object
});

module.exports = mongoose.model('TaskCreate', modelSchema);

