"use strict";

const mongoose = require("mongoose");

const modelSchema = mongoose.Schema({
  createdAt: { type: String, default: new Date() },
  updatedAt: { type: String, default: new Date() },
  protocolId: { type: String, ref: "Protocol" }, // Protocol parent

  // Input Output
  sample_input: { type: Array, required: true },
  output: { type: Array, required: true },

  // Instruction Task
  instruction_input: { type: Array, required: true },
  instruction: { type: String, required: true },
  
  // Control Flow
  previousInstruction: { type: Array, required: true },
  nextInstruction: { type: Array, required: true },

});

module.exports = mongoose.model("Instruction", modelSchema);

  
