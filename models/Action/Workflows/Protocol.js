"use strict";

const mongoose = require("mongoose");

const protocolSchema = mongoose.Schema({
  master: Boolean,
  createdAt: { type: String, default: new Date() },
  updatedAt: { type: String, default: new Date() },
  author: String, // Link to profile
  name: String,
  description: String,

  inputs: { type: Array, required: true},
  outputs: { type: Array, required: true},
  });

module.exports = mongoose.model("Protocol", protocolSchema);
