'use strict'

const mongoose = require('mongoose');

const modelSchema = mongoose.Schema({
  createdAt    : { type: String, default: new Date() },
  updatedAt    : { type: String, default: new Date() },
  name         : { type: String, required: true },
  status       : { type: String, default: "Pending" },
  requesterAffiliation : { type: String, required: true },
  requesterEmail : { type: String, required: true },
  requesterName : { type: String, required: true },
  requesterOrcid : { type: String, required: true },
  shippingAddress: { type: String, required: true },
  action: { type: Object }, // Requires workflow or protocol
  inputs: { type: Array },
});

module.exports = mongoose.model('RequestMaterial', modelSchema);

