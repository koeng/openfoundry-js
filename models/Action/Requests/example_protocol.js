'use strict'

const mongoose = require('mongoose');

const modelSchema = mongoose.Schema({
  createdAt    : { type: String, default: new Date() },
  updatedAt    : { type: String, default: new Date() },
  name         : { type: String, required: true },
  status       : { type: String, default: "Pending" },
  requesterAffiliation : { type: String. required: true },
  requesterEmail : { type: String. required: true },
  requesterName : { type: String. required: true },
  requesterOrcid : { type: String. required: true },
  shippingAddress: { type: String. required: true },

  sequence : String,
  part_type: { type: String, required: true },
  source_organism: { type: String, required: true },
  target_organism: { type: String, required: true }, // Limited number of organisms
  description: { type: String, required: true }, // Markdown file
  links: { type: Array, required: true }, // List of internet links

  project_name: { type: String, required: true },
  safety_information: { type: String, required: true },
  collection: { type: Number, required: true }, // Collection number to identify groups of requests

});

module.exports = mongoose.model('RequestSynthesis', modelSchema);


