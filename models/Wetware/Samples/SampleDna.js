"use strict";

const mongoose = require("mongoose");

const modelSchema = mongoose.Schema({
  createdAt: { type: String, default: new Date() },
  updatedAt: { type: String, default: new Date() },
  parentId: { type: String, ref: "Well", required: true },
  name: { type: String, required: true },
  description: String,

  // Provenance tracking
  provenance: { type: String }, // Human readable provenance
  provenanceId:  { type: String, required: true },
  provenanceJob:  { type: String, required: true },

  // Associated physical data
  associatedFiles: { type: String }, // Meant for sequencing files 
  mta: { type: String }, // MTA needed for physical material
  tags: [{type: Array }],

  // Link to Part, Plasmid, or Vector virtual
  dna: { type: String, required: true },

  // DNA specific data
  quantity: Number,
  quantityDeterminedBy: String, // Enum
  quantityJob: String,

  quality: Number,
  qualityDeterminedBy: String, // Enum 
  qualityJob: String,
});

module.exports = mongoose.model("SampleDna", modelSchema);

