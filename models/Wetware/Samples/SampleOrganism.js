"use strict";

const mongoose = require("mongoose");

const modelSchema = mongoose.Schema({
  createdAt: { type: String, default: new Date() },
  updatedAt: { type: String, default: new Date() },
  parentId: { type: String, ref: "Well", required: true },
  name: { type: String, required: true },
  description: String,
  
  // Provenance tracking
  provenance: { type: String }, // Human readable provenance
  provenanceId:  { type: String, required: true },
  provenanceJob:  { type: String, required: true },

  // Associated physical data
  associated_files: { type: String }, // Meant for sequencing files 
  mta: { type: String }, // MTA needed for physical material
  tags: [{type: Array }],

  // Link to Organism virtual
  organism: { type: String, required: true },

});

module.exports = mongoose.model("SampleOrganism", modelSchema);

