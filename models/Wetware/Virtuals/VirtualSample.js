"use strict";

const mongoose = require("mongoose");

const modelSchema = mongoose.Schema({
  // Metadata
  createdAt: { type: String, default: new Date() },
  updatedAt: { type: String, default: new Date() },

  name: { type: String, required: true },
  description: { type: String, required: true },
  status: { type: String, default: "Pending" },
  authorAffiliation: { type: String, required: true },
  authorEmail: { type: String, required: true },
  authorName: { type: String, required: true },
  authorOrcid: { type: String, required: true },

  // Information about this sample
  datName: String,
  datHash: String,

  // Provenance
  createdByJob: { type: String, required: true },
  derivedFrom: { type: Array, required: true }, // Other samples
  originNode: { type: String, required: true }
});

module.exports = mongoose.model("VirtualSample", modelSchema);

// Vast majority of the documentation should be in the dat, not stored locally in this file.
