"use strict";

const mongoose = require("mongoose");

const modelSchema = mongoose.Schema({
  // Metadata
  updatedAt: { type: String, default: new Date() },
  createdAt: { type: String, default: new Date() },

  author: { type: String, required: true },
  name: { type: String, required: true },
  description: { type: String, required: true },
  status: { type: String, default: "Pending" },
  authorAffiliation: { type: String, required: true },
  authorEmail: { type: String, required: true },
  authorName: { type: String, required: true },
  authorOrcid: { type: String, required: true },

  // Information about this vector
  sequence: { type: String, required: true },
  cloningMethod: String,
  tags: [],
  functionsIn: [],
  part: { type: String, required: true },
  vector: { type: String, required: true },
  datName: String,
  datHash: String,

  // Provenance
  derivedFrom: { type: Array, required: true } // Other parts
});

module.exports = mongoose.model("VirtualPlasmid", modelSchema);

// Vast majority of the documentation should be in the dat, not stored locally in this file.
