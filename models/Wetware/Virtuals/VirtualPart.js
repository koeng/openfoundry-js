"use strict";

const mongoose = require("mongoose");

const modelSchema = mongoose.Schema({
  // Metadata
  createdAt: { type: String, default: new Date() },
  updatedAt: { type: String, default: new Date() },
  name: { type: String, required: true },
  description: { type: String, required: true },
  status: { type: String, default: "Pending" },
  author: { type: String, ref: "User", required: true },
  authorAffiliation: String,
  authorEmail: { type: String, required: true },
  authorName: { type: String, required: true },
  authorOrcid: String,

  // Information about this part
  targetOrganism: String,
  sourceOrganism: String,
  partType: { type: String, required: true },
  sequence: { type: String, required: true },
  datName: String,
  datHash: String,

  tags: Array,

  // Provenance
  //createdByJob : { type: String, required: true },
  derivedFrom: Array // Other parts
  //originNode : { type: String, required: true },
});

module.exports = mongoose.model("VirtualPart", modelSchema);

// Vast majority of the documentation should be in the dat, not stored locally in this file.
