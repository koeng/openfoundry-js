"use strict";

const mongoose = require("mongoose");

const modelSchema = mongoose.Schema({
  // Metadata
  createdAt: { type: String, default: new Date() },
  updatedAt: { type: String, default: new Date() },

  authorAffiliation: { type: String, required: true },
  authorEmail: { type: String, required: true },
  authorName: { type: String, required: true },
  authorOrcid: { type: String, required: true },
  name: { type: String, required: true },
  description: { type: String, required: true },
  status: { type: String, default: "Pending" },

  // Information about this vector
  targetOrganisms: Array,
  sequence: { type: String, required: true },

  // Conditions
  temperature: Number,
  positiveSelection: Array, // Tuple of marker and ug/mL (Ampicillin, 100)
  negativeSelection: Array, // Tuple of marker and ug/mL (Sucrose, 100)
  genotypeConditions: Array, // ("ccdb", "fplasmid", "rplasmid", etc). Allows for selection of strains needed by different vectors.
  media: Array, // 2YT, LB, etc. In order of preference.

  // Provenance
  createdByJob: { type: String, required: true },
  derivedFrom: { type: Array, required: true }, // Other parts
  originNode: { type: String, required: true }
});

module.exports = mongoose.model("VirtualVector", modelSchema);

// Vast majority of the documentation should be in the dat, not stored locally in this file.
