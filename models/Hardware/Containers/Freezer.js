"use strict";

const mongoose = require("mongoose");

const modelSchema = mongoose.Schema({
  createdAt: { type: String, default: new Date() },
  updatedAt: { type: String, default: new Date() },
  parentId: { type: String, ref: "Lab", required: true },
  name: { type: String, required: true },
  description: String,
  temperature: { type: Number, required: true },
  potentialLocation: [{ type: Array }],
  parentLocation: String,
  canContain: {
    type: Array,
    default: ["Plate", "Tube", "Container"]
  },

  lastDefrost: { type: String, default: new Date() },
});

module.exports = mongoose.model("Freezer", modelSchema);
