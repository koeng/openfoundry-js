"use strict";

const mongoose = require("mongoose");

const wellSchema = mongoose.Schema({
  createdAt: { type: String, default: new Date() },
  updatedAt: { type: String, default: new Date() },
  parentId: { type: String, ref: "Plate", required: true },
  parentLocation: [{ type: Array }],
  name: { type: String, required: true },
  description: String,
  volume: Number,
  wellType: { type: String, required: true },
  parentLocation: String,
  canContain: {
    type: Array,
    default: ["Sample", "OrganismSample", "DNASample"] 
  }

});

module.exports = mongoose.model("Well", wellSchema);
