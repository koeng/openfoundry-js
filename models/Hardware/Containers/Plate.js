"use strict";

const mongoose = require("mongoose");

const plateSchema = mongoose.Schema({
  createdAt: { type: String, default: new Date() },
  updatedAt: { type: String, default: new Date() },
  parentId: { type: String, ref: "Freezer", required: true },
  parentLocation: [{ type: Array }],
  name: { type: String, required: true },
  description: String,
  potentialLocation: [{ type: Array }], 
  storageTemperature: Number,
  plateType: { type: String, required: true }
});

module.exports = mongoose.model("Plate", plateSchema);
