"use strict";

const mongoose = require("mongoose");

const modelSchema = mongoose.Schema({
  createdAt: { type: String, default: new Date() },
  updatedAt: { type: String, default: new Date() },
  parentId: { type: String, ref: "Plate", required: true },
  parentLocation: [{ type: Array }],
  name: { type: String, required: true },
  description: String,
  potentialLocation: [{ type: Array }],
  parentLocation: String,


  benchType: { type: String, required: true }, // OT2, Technician, Tecan
  canContain: {
    type: Array,
    default: ["Plates, TipRacks, TipRack2ml"]
  },
  abilities: {
    type: Array,
    default: ["multichannel-p300, multichannel-p10"]
  },

});

module.exports = mongoose.model("Bench", modelSchema);

