"use strict";

const mongoose = require("mongoose");

const incubateSchema = mongoose.Schema({
  createdAt: { type: String, default: new Date() },
  protocolId: { type: String, ref: "Protocol" }, // Protocol parent
  name: String,
  description: String,
  objects_in_inventory: Array // Array of UUIDs of things in this inventory
  restock_notice: Number // Restock when there are this many objects left in array
});

module.exports = mongoose.model("Inventory", incubateSchema);

