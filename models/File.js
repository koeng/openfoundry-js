"use strict";

const mongoose = require("mongoose");

const modelSchema = mongoose.Schema({
  // Metadata
  createdAt: { type: String, default: new Date() },
  updatedAt: { type: String, default: new Date() },
  file: { type: String, required: true },
  fileName: { type: String, required: true },
  fileType: { type: String, required: true },

  // Information about this organism
  datName: String,
  datHash: String,
});

module.exports = mongoose.model("File", modelSchema);


