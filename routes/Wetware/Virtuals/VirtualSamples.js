const User = require("../../../models/User");
const VirtualSample = require("../../../models/Wetware/Virtuals/VirtualSample");
const jwt = require("jsonwebtoken");
const adminRequired = require("../../../modules/apiAccess").adminRequired;

if (!process.env.JWT_SECRET) {
  require("../config/env.js");
}

module.exports = function(app) {
  // create new record
  app.post("/virtualsamples/new", adminRequired, (req, res) => {
    let newRecord = new VirtualSample({
      name: req.body.name,
      description: req.body.description,
      authorAffiliation: req.body.authorAffiliation,
      authorEmail: req.body.authorEmail,
      authorName: req.body.authorName,
      authorOrcid: req.body.authorOrcid,
      datName: req.body.datName,
      datHash: req.body.datHash,
      createdByJob: req.body.createdByJob,
      derivedFrom: req.body.derivedFrom,
      originNode: req.body.originNode
    });
    newRecord.save((error, data) => {
      let jsonResponse;
      if (error) {
        jsonResponse = {
          message: "There was a problem saving the new record.",
          data: {},
          error
        };
      } else {
        jsonResponse = {
          message: "The new record was successfully saved.",
          data: data,
          error: {}
        };
      }
      res.json(jsonResponse);
    });
  });

  // remove record
  app.post("/virtualsamples/:recordId/remove", adminRequired, (req, res) => {
    VirtualSample.findByIdAndRemove(req.params.recordId).exec(error => {
      if (error) {
        jsonResponse = {
          message: "There was a problem removing the record."
        };
      } else {
        jsonResponse = {
          message: "The record was successfully removed."
        };
      }
      res.json(jsonResponse);
    });
  });

  // edit record
  app.post("/virtualsamples/:recordId/edit", adminRequired, (req, res) => {
    if (process.env.NODE_ENV === "test") {
      VirtualSample.findOne({ _id: req.params.recordId }).exec(
        (err, record) => {
          record.updatedAt = new Date();
          record.createdAt = new Date();

          record.name = req.body.name;
          record.description = req.body.description;
          record.authorAffiliation = req.body.authorAffiliation;
          record.authorEmail = req.body.authorEmail;
          record.authorName = req.body.authorName;
          record.authorOrcid = req.body.authorOrcid;
          record.datName = req.body.datName;
          record.datHash = req.body.datHash;
          record.createdByJob = req.body.createdByJob;
          record.derivedFrom = req.body.derivedFrom;
          record.orgiginNode = req.body.orgiginNode;

          record.save((error, updatedRecord) => {
            let jsonResponse;
            if (error) {
              jsonResponse = {
                message: "There was a problem saving the updated record.",
                data: record
              };
            } else {
              jsonResponse = {
                message: "The updated record was successfully saved.",
                data: updatedRecord
              };
            }
            res.json(jsonResponse);
          });
        }
      );
    } else {
      VirtualSample.findOne({ _id: req.params.recordId }).exec(
        (err, record) => {
          record.updatedAt = new Date();
          record.createdAt = new Date();

          record.name = req.body.name;
          record.description = req.body.description;
          record.authorAffiliation = req.body.authorAffiliation;
          record.authorEmail = req.body.authorEmail;
          record.authorName = req.body.authorName;
          record.authorOrcid = req.body.authorOrcid;
          record.datName = req.body.datName;
          record.datHash = req.body.datHash;
          record.createdByJob = req.body.createdByJob;
          record.derivedFrom = req.body.derivedFrom;
          record.orgiginNode = req.body.orgiginNode;

          record.save((error, updatedRecord) => {
            let jsonResponse;
            if (error) {
              jsonResponse = {
                message: "There was a problem saving the updated record.",
                data: record
              };
            } else {
              jsonResponse = {
                message: "The updated record was successfully saved.",
                data: updatedRecord
              };
            }
            res.json(jsonResponse);
          });
        }
      );
    }
  });

  // show one record
  app.get("/virtualsamples/:recordId", getRecordById, (req, res) => {
    let jsonResponse = {
      message: res.locals.message,
      data: res.locals.data
    };
    res.json(jsonResponse);
  });

  // list all records
  app.get("/virtualsamples", getAllRecords, (req, res) => {
    let jsonResponse = {
      message: res.locals.message,
      data: res.locals.data
    };
    res.json(jsonResponse);
  });
};

function getAllRecords(req, res, next) {
  if (process.env.NODE_ENV === "test") {
    VirtualSample.find({}, {}, { sort: { name: 1 } }).exec((error, data) => {
      if (error) {
        res.locals.message = "There was a problem with retrieving the records.";
      } else {
        res.locals.message = "The records were successfully retrieved.";
      }
      res.locals.data = data;
      return next();
    });
  } else {
    VirtualSample.find({}, {}, { sort: { name: 1 } }).exec((error, data) => {
      if (error) {
        res.locals.message = "There was a problem with retrieving the records.";
      } else {
        res.locals.message = "The records were successfully retrieved.";
      }
      res.locals.data = data;
      return next();
    });
  }
}

function getRecordById(req, res, next) {
  if (process.env.NODE_ENV === "test") {
    VirtualSample.findOne({ _id: req.params.recordId }).exec((error, data) => {
      if (error) {
        res.locals.message = "There was a problem with retrieving the record.";
        res.locals.data = {};
      } else {
        res.locals.message = "The record was successfully retrieved.";
        res.locals.data = data;
      }
      return next();
    });
  } else {
    VirtualSample.findOne({ _id: req.params.recordId }).exec((error, data) => {
      if (error) {
        res.locals.message = "There was a problem with retrieving the record.";
        res.locals.data = {};
      } else {
        res.locals.message = "The record was successfully retrieved.";
        res.locals.data = data;
      }
      return next();
    });
  }
}
