const User = require("../../../models/User");
const VirtualOrganism = require("../../../models/Wetware/Virtuals/VirtualOrganism");
const jwt = require("jsonwebtoken");
const adminRequired = require("../../../modules/apiAccess").adminRequired;

if (!process.env.JWT_SECRET) {
  require("../config/env.js");
}

module.exports = function(app) {
  // create new record
  app.post("/virtualorganisms/new", adminRequired, (req, res) => {
    let newRecord = new VirtualOrganism({
      author: res.locals.currentUser || req.body.author,
      name: req.body.name,
      description: req.body.description,
      authorAffiliation: req.body.authorAffiliation,
      authorEmail: req.body.authorEmail,
      authorName: req.body.authorName,
      authorOrcid: req.body.authorOrcid,
      taxid: req.body.taxid,
      temperature: req.body.temperature,
      positiveSelection: req.body.positiveSelection,
      negativeSelection: req.body.negativeSelection,
      genotypeConditions: req.body.genotypeConditions,
      media: req.body.media,
      datName: req.body.datName,
      datHash: req.body.datHash,
      derivedFrom: req.body.derivedFrom
    });
    newRecord.save((error, data) => {
      let jsonResponse;
      if (error) {
        jsonResponse = {
          message: "There was a problem saving the new record.",
          data: {},
          error
        };
      } else {
        jsonResponse = {
          message: "The new record was successfully saved.",
          data: data,
          error: {}
        };
      }
      res.json(jsonResponse);
    });
  });

  // remove record
  app.post("/virtualorganisms/:recordId/remove", adminRequired, (req, res) => {
    VirtualOrganism.findByIdAndRemove(req.params.recordId).exec(error => {
      if (error) {
        jsonResponse = {
          message: "There was a problem removing the record."
        };
      } else {
        jsonResponse = {
          message: "The record was successfully removed."
        };
      }
      res.json(jsonResponse);
    });
  });

  // edit record
  app.post("/virtualorganisms/:recordId/edit", adminRequired, (req, res) => {
    if (process.env.NODE_ENV === "test") {
      VirtualOrganism.findOne({ _id: req.params.recordId }).exec(
        (err, record) => {
          record.author = req.body.author;
          record.name = req.body.name;
          record.description = req.body.description;
          record.authorAffiliation = req.body.authorAffiliation;
          record.authorEmail = req.body.authorEmail;
          record.authorName = req.body.authorName;
          record.authorOrcid = req.body.authorOrcid;
          record.taxid = req.body.taxid;
          record.temperature = req.body.temperature;
          record.positiveSelection = req.body.temperature;
          record.negativeSelection = req.body.negativeSelection;
          record.genotypeConditions = req.body.genotypeConditions;
          record.media = req.body.array;
          record.createdByJob = req.body.createdByJob;
          record.datName = req.body.datName;
          record.datHash = req.body.datHash;
          // record.derivedForm = req.body.derivedFrom;
          // record.orginNode = req.body.originNode;

          record.save((error, updatedRecord) => {
            let jsonResponse;
            if (error) {
              jsonResponse = {
                message: "There was a problem saving the updated record.",
                data: record
              };
            } else {
              jsonResponse = {
                message: "The updated record was successfully saved.",
                data: updatedRecord
              };
            }
            res.json(jsonResponse);
          });
        }
      );
    } else {
      VirtualOrganism.findOne({ _id: req.params.recordId }).exec(
        (err, record) => {
          record.author = req.body.author;
          record.name = req.body.name;
          record.description = req.body.description;
          record.authorAffiliation = req.body.authorAffiliation;
          record.authorEmail = req.body.authorEmail;
          record.authorName = req.body.authorName;
          record.authorOrcid = req.body.authorOrcid;
          record.taxid = req.body.taxid;
          record.temperature = req.body.temperature;
          record.positiveSelection = req.body.temperature;
          record.negativeSelection = req.body.negativeSelection;
          record.genotypeConditions = req.body.genotypeConditions;
          record.media = req.body.array;
          record.createdByJob = req.body.createdByJob;
          record.datName = req.body.datName;
          record.datHash = req.body.datHash;

          record.save((error, updatedRecord) => {
            let jsonResponse;
            if (error) {
              jsonResponse = {
                message: "There was a problem saving the updated record.",
                data: record
              };
            } else {
              jsonResponse = {
                message: "The updated record was successfully saved.",
                data: updatedRecord
              };
            }
            res.json(jsonResponse);
          });
        }
      );
    }
  });

  // show one record
  app.get("/virtualorganisms/:recordId", getRecordById, (req, res) => {
    let jsonResponse = {
      message: res.locals.message,
      data: res.locals.data
    };
    res.json(jsonResponse);
  });

  // list all records
  app.get("/virtualorganisms", getAllRecords, (req, res) => {
    let jsonResponse = {
      message: res.locals.message,
      data: res.locals.data
    };
    res.json(jsonResponse);
  });
};

function getAllRecords(req, res, next) {
  if (process.env.NODE_ENV === "test") {
    VirtualOrganism.find({}, {}, { sort: { name: 1 } }).exec((error, data) => {
      if (error) {
        res.locals.message = "There was a problem with retrieving the records.";
      } else {
        res.locals.message = "The records were successfully retrieved.";
      }
      res.locals.data = data;
      return next();
    });
  } else {
    VirtualOrganism.find({}, {}, { sort: { name: 1 } }).exec((error, data) => {
      if (error) {
        res.locals.message = "There was a problem with retrieving the records.";
      } else {
        res.locals.message = "The records were successfully retrieved.";
      }
      res.locals.data = data;
      return next();
    });
  }
}

function getRecordById(req, res, next) {
  if (process.env.NODE_ENV === "test") {
    VirtualOrganism.findOne({ _id: req.params.recordId }).exec(
      (error, data) => {
        if (error) {
          res.locals.message =
            "There was a problem with retrieving the record.";
          res.locals.data = {};
        } else {
          res.locals.message = "The record was successfully retrieved.";
          res.locals.data = data;
        }
        return next();
      }
    );
  } else {
    VirtualOrganism.findOne({ _id: req.params.recordId }).exec(
      (error, data) => {
        if (error) {
          res.locals.message =
            "There was a problem with retrieving the record.";
          res.locals.data = {};
        } else {
          res.locals.message = "The record was successfully retrieved.";
          res.locals.data = data;
        }
        return next();
      }
    );
  }
}
