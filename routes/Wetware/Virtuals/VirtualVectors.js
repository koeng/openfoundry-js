const User = require("../../../models/User");
const VirtualVector = require("../../../models/Wetware/Virtuals/VirtualVector");
const jwt = require("jsonwebtoken");
const adminRequired = require("../../../modules/apiAccess").adminRequired;

if (!process.env.JWT_SECRET) {
  require("../config/env.js");
}

module.exports = function(app) {
  // create new record
  app.post("/virtualvectors/new", adminRequired, (req, res) => {
    let newRecord = new VirtualVector({
      authorAffiliation: req.body.authorAffiliation,
      authorEmail: req.body.authorEmail,
      authorName: req.body.authorName,
      authorOrcid: req.body.authorOrcid,
      name: req.body.name,
      description: req.body.description,
      status: req.body.status,
      targetOrganisms: req.body.targetOrganisms,
      sequence: req.body.sequence,
      temperature: req.body.temperature,
      positiveSelection: req.body.positiveSelection,
      negativeSelection: req.body.negativeSelection,
      genotypeConditions: req.body.genotypeConditions,
      media: req.body.media,
      createdByJob: req.body.createdByJob,
      derivedFrom: req.body.derivedFrom,
      originNode: req.body.originNode
    });
    newRecord.save((error, data) => {
      let jsonResponse;
      if (error) {
        jsonResponse = {
          message: "There was a problem saving the new record.",
          data: {},
          error
        };
      } else {
        jsonResponse = {
          message: "The new record was successfully saved.",
          data: data,
          error: {}
        };
      }
      res.json(jsonResponse);
    });
  });

  // remove record
  app.post("/virtualvectors/:recordId/remove", adminRequired, (req, res) => {
    VirtualVector.findByIdAndRemove(req.params.recordId).exec(error => {
      if (error) {
        jsonResponse = {
          message: "There was a problem removing the record."
        };
      } else {
        jsonResponse = {
          message: "The record was successfully removed."
        };
      }
      res.json(jsonResponse);
    });
  });

  // edit record
  app.post("/virtualvectors/:recordId/edit", adminRequired, (req, res) => {
    if (process.env.NODE_ENV === "test") {
      VirtualVector.findOne({ _id: req.params.recordId }).exec(
        (err, record) => {
          record.createddAt = new Date();
          record.updatedAt = new Date();
          record.authorAffiliation = req.body.authorAffiliation;
          record.authorEmail = req.body.authorEmail;
          record.authorName = req.body.authorName;
          record.authorOrcid = req.body.authorOrcid;
          record.name = req.body.name;
          record.description = req.body.description;
          record.status = req.body.status;
          record.targetOrganisms = req.body.targetOrganisms;
          record.sequence = req.body.sequence;
          record.temperature = req.body.temperature;
          record.positiveSelection = req.body.positiveSelection;
          record.negativeSelection = req.body.negativeSelection;
          record.genotypeConditions = req.body.genotypeConditions;
          record.media = req.body.media;
          record.createdByJob = req.body.createdByJob;
          record.derivedFrom = req.body.derivedFrom;
          record.originNode = req.body.originNode;

          record.save((error, updatedRecord) => {
            let jsonResponse;
            if (error) {
              jsonResponse = {
                message: "There was a problem saving the updated record.",
                data: record
              };
            } else {
              jsonResponse = {
                message: "The updated record was successfully saved.",
                data: updatedRecord
              };
            }
            res.json(jsonResponse);
          });
        }
      );
    } else {
      VirtualVector.findOne({ _id: req.params.recordId }).exec(
        (err, record) => {
          record.createddAt = new Date();
          record.updatedAt = new Date();
          record.authorAffiliation = req.body.authorAffiliation;
          record.authorEmail = req.body.authorEmail;
          record.authorName = req.body.authorName;
          record.authorOrcid = req.body.authorOrcid;
          record.name = req.body.name;
          record.description = req.body.description;
          record.status = req.body.status;
          record.targetOrganisms = req.body.targetOrganisms;
          record.sequence = req.body.sequence;
          record.temperature = req.body.temperature;
          record.positiveSelection = req.body.positiveSelection;
          record.negativeSelection = req.body.negativeSelection;
          record.genotypeConditions = req.body.genotypeConditions;
          record.media = req.body.media;
          record.createdByJob = req.body.createdByJob;
          record.derivedFrom = req.body.derivedFrom;
          record.originNode = req.body.originNode;

          record.save((error, updatedRecord) => {
            let jsonResponse;
            if (error) {
              jsonResponse = {
                message: "There was a problem saving the updated record.",
                data: record
              };
            } else {
              jsonResponse = {
                message: "The updated record was successfully saved.",
                data: updatedRecord
              };
            }
            res.json(jsonResponse);
          });
        }
      );
    }
  });

  // show one record
  app.get("/virtualvectors/:recordId", getRecordById, (req, res) => {
    let jsonResponse = {
      message: res.locals.message,
      data: res.locals.data
    };
    res.json(jsonResponse);
  });

  // list all records
  app.get("/virtualvectors", getAllRecords, (req, res) => {
    let jsonResponse = {
      message: res.locals.message,
      data: res.locals.data
    };
    res.json(jsonResponse);
  });
};

function getAllRecords(req, res, next) {
  if (process.env.NODE_ENV === "test") {
    VirtualVector.find({}, {}, { sort: { name: 1 } }).exec((error, data) => {
      if (error) {
        res.locals.message = "There was a problem with retrieving the records.";
      } else {
        res.locals.message = "The records were successfully retrieved.";
      }
      res.locals.data = data;
      return next();
    });
  } else {
    VirtualVector.find({}, {}, { sort: { name: 1 } }).exec((error, data) => {
      if (error) {
        res.locals.message = "There was a problem with retrieving the records.";
      } else {
        res.locals.message = "The records were successfully retrieved.";
      }
      res.locals.data = data;
      return next();
    });
  }
}

function getRecordById(req, res, next) {
  if (process.env.NODE_ENV === "test") {
    VirtualVector.findOne({ _id: req.params.recordId }).exec((error, data) => {
      if (error) {
        res.locals.message = "There was a problem with retrieving the record.";
        res.locals.data = {};
      } else {
        res.locals.message = "The record was successfully retrieved.";
        res.locals.data = data;
      }
      return next();
    });
  } else {
    VirtualVector.findOne({ _id: req.params.recordId }).exec((error, data) => {
      if (error) {
        res.locals.message = "There was a problem with retrieving the record.";
        res.locals.data = {};
      } else {
        res.locals.message = "The record was successfully retrieved.";
        res.locals.data = data;
      }
      return next();
    });
  }
}
