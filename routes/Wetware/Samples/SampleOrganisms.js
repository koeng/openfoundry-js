const User = require("../../../models/User");
const Well = require("../../../models/Hardware/Containers/Well");
const OrganismSample = require("../../../models/Wetware/Samples/SampleOrganism");
const jwt = require("jsonwebtoken");
const adminRequired = require("../../../modules/apiAccess").adminRequired;

if (!process.env.JWT_SECRET) {
  require("../../../config/env.js");
}

module.exports = function(app) {
  // create new record
  app.post("/organism-samples/new", adminRequired, (req, res) => {
    let newRecord = new OrganismSample({
      parentId: req.body.parentId,
      name: req.body.name,
      description: req.body.description,
      provenance: req.body.provenance,
      provenanceId: req.body.provenanceId,
      provenanceJob: req.body.provenanceJob,
      mta: req.body.mta,
      organism: req.body.organism
    });
    newRecord.save((error, data) => {
      let jsonResponse;
      if (error) {
        jsonResponse = {
          message: "There was a problem saving the new record.",
          data: {},
          error
        };
      } else {
        jsonResponse = {
          message: "The new record was successfully saved.",
          data: data,
          error: {}
        };
      }
      res.json(jsonResponse);
    });
  });

  // remove record
  app.post("/organism-samples/:recordId/remove", adminRequired, (req, res) => {
    OrganismSample.findByIdAndRemove(req.params.recordId).exec(error => {
      if (error) {
        jsonResponse = {
          message: "There was a problem removing the record."
        };
      } else {
        jsonResponse = {
          message: "The record was successfully removed."
        };
      }
      res.json(jsonResponse);
    });
  });

  // edit record
  app.post("/organism-samples/:recordId/edit", adminRequired, (req, res) => {
    OrganismSample.findOne({ _id: req.params.recordId })
      .populate("parentId")
      .exec((err, record) => {
        record.name = req.body.name;
        record.description = req.body.description;
        record.provenance = req.body.provenance;
        record.mta = req.body.mta;
        record.updatedAt = new Date();
    
        record.save((error, updatedRecord) => {
          let jsonResponse;
          if (error) {
            jsonResponse = {
              message: "There was a problem saving the updated record.",
              data: record
            };
          } else {
            jsonResponse = {
              message: "The updated record was successfully saved.",
              data: updatedRecord
            };
          }
          res.json(jsonResponse);
        });
      });
  });

  // show one record
  app.get("/organism-samples/:recordId", getRecordById, (req, res) => {
    let jsonResponse = {
      message: res.locals.message,
      data: res.locals.data
    };
    res.json(jsonResponse);
  });

  // list all records
  app.get("/organism-samples", getAllRecords, (req, res) => {
    let jsonResponse = {
      message: res.locals.message,
      data: res.locals.data
    };
    res.json(jsonResponse);
  });
};

function getAllRecords(req, res, next) {
  OrganismSample.find({}, {}, { sort: { name: 1 } })
    .populate("parentId")
    .exec((error, data) => {
      if (error) {
        res.locals.message =
          "There was a problem with retrieving the records.";
      } else {
        res.locals.message =
          "The records were successfully retrieved.";
      }
      res.locals.data = data;
      return next();
    });
}

function getRecordById(req, res, next) {
	OrganismSample
  .findOne({'_id': req.params.recordId})
  .populate("parentId")
	.exec((error, data) => {
		if(error) {
      res.locals.message = "There was a problem with retrieving the record.";
      res.locals.data = {};
		} else {
      res.locals.message = "The record was successfully retrieved.";
      res.locals.data = data;               
    }
    return next();
  });
}
