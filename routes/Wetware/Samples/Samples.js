const User = require("../../../models/User");
const Well = require("../../../models/Hardware/Containers/Well");
const Sample = require("../../../models/Wetware/Samples/Sample");
const jwt = require("jsonwebtoken");
const adminRequired = require("../../../modules/apiAccess").adminRequired;

if (!process.env.JWT_SECRET) {
  require("../../../config/env.js");
}

module.exports = function(app) {
  // create new record
  app.post("/samples/new", adminRequired, (req, res) => {
    let newRecord = new Sample({
      parentId: req.body.parentId,
      name: req.body.name,
      description: req.body.description,
      provenance: req.body.provenance,
      provenanceId: req.body.provenanceId,
      provenanceJob: req.body.provenanceJob,
      mta: req.body.mta,
      sample: req.body.sample
    });
    newRecord.save((error, data) => {
      let jsonResponse;
      if (error) {
        jsonResponse = {
          message: "There was a problem saving the new record.",
          data: {},
          error
        };
      } else {
        jsonResponse = {
          message: "The new record was successfully saved.",
          data: data,
          error: {}
        };
      }
      res.json(jsonResponse);
    });
  });

  // remove record
  app.post("/samples/:recordId/remove", adminRequired, (req, res) => {
    Sample.findByIdAndRemove(req.params.recordId).exec(error => {
      if (error) {
        jsonResponse = {
          message: "There was a problem removing the record."
        };
      } else {
        jsonResponse = {
          message: "The record was successfully removed."
        };
      }
      res.json(jsonResponse);
    });
  });

  // edit record
  app.post("/samples/:recordId/edit", adminRequired, (req, res) => {
    Sample.findOne({ _id: req.params.recordId })
      .populate("parentId")
      .exec((err, record) => {
        record.name = req.body.name;
        record.description = req.body.description;
        record.provenance = req.body.provenance;
        record.mta = req.body.mta;
        record.updateAt = new Date();
    
        record.save((error, updatedRecord) => {
          let jsonResponse;
          if (error) {
            jsonResponse = {
              message: "There was a problem saving the updated record.",
              data: record
            };
          } else {
            jsonResponse = {
              message: "The updated record was successfully saved.",
              data: updatedRecord
            };
          }
          res.json(jsonResponse);
        });
      });
  });

  // show one record
  app.get("/samples/:recordId", getRecordById, (req, res) => {
    let jsonResponse = {
      message: res.locals.message,
      data: res.locals.data
    };
    res.json(jsonResponse);
  });

  // list all records
  app.get("/samples", getAllRecords, (req, res) => {
    let jsonResponse = {
      message: res.locals.message,
      data: res.locals.data
    };
    res.json(jsonResponse);
  });
};

function getAllRecords(req, res, next) {
  Sample.find({}, {}, { sort: { name: 1 } })
    .populate("parentId")
    .exec((error, data) => {
      if (error) {
        res.locals.message =
          "There was a problem with retrieving the records.";
      } else {
        res.locals.message =
          "The records were successfully retrieved.";
      }
      res.locals.data = data;
      return next();
    });
}

function getRecordById(req, res, next) {
	Sample
  .findOne({'_id': req.params.recordId})
  .populate("parentId")
	.exec((error, data) => {
		if(error) {
      res.locals.message = "There was a problem with retrieving the record.";
      res.locals.data = {};
		} else {
      res.locals.message = "The record was successfully retrieved.";
      res.locals.data = data;               
    }
    return next();
  });
}
