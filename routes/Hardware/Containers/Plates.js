const User = require("../../../models/User");
const Freezer = require("../../../models/Hardware/Containers/Freezer");
const Plate = require("../../../models/Hardware/Containers/Plate");
const Well = require("../../../models/Hardware/Containers/Well");
const jwt = require("jsonwebtoken");
const adminRequired = require("../../../modules/apiAccess").adminRequired;

if (!process.env.JWT_SECRET) {
  require("../../../config/env.js");
}

module.exports = function(app) {
  // create new record
  app.post("/plates/new", adminRequired, (req, res) => {
    let newRecord = new Plate({
      parentId: req.body.parentId,
      name: req.body.name,
      description: req.body.description,
      storageTemperature: req.body.storageTemperature,
      plateType: req.body.plateType
    });
    newRecord.save((error, data) => {
      let jsonResponse;
      if (error) {
        jsonResponse = {
          message: "There was a problem saving the new record.",
          data: {},
          error
        };
      } else {
        jsonResponse = {
          message: "The new record was successfully saved.",
          data: data,
          error: {}
        };
      }
      res.json(jsonResponse);
    });
  });

  // add new child inside record
  app.post("/plates/:recordId/new-well", adminRequired, (req, res) => {
    let newChild = new Well({
      parentId: req.body.parentId,
      name: req.body.name,
      description: req.body.description,
      volume: req.body.volume
    });
    newChild.save((error, data) => {
      let jsonResponse;
      if (error) {
        jsonResponse = {
          message: "There was a problem saving the new record.",
          data: {}
        };
      } else {
        jsonResponse = {
          message: "The new record was successfully saved.",
          data: data
        };
      }
      res.json(jsonResponse);
    });
  });

  // remove record
  app.post("/plates/:recordId/remove", adminRequired, (req, res) => {
    Plate.findByIdAndRemove(req.params.recordId).exec(error => {
      if (error) {
        jsonResponse = {
          message: "There was a problem removing the record."
        };
      } else {
        jsonResponse = {
          message: "The record was successfully removed."
        };
      }
      res.json(jsonResponse);
    });
  });

  // edit record
  app.post("/plates/:recordId/edit", adminRequired, (req, res) => {
    Plate.findOne({ _id: req.params.recordId })
      .populate("parentId")
      .exec((err, record) => {
        record.name = req.body.name;
        record.description = req.body.description;
        record.storageTemperature = req.body.storageTemperature;
        record.plateType = req.body.plateType;
        record.updateAt = new Date();
    
        record.save((error, updatedRecord) => {
          let jsonResponse;
          if (error) {
            jsonResponse = {
              message: "There was a problem saving the updated record.",
              data: record
            };
          } else {
            jsonResponse = {
              message: "The updated record was successfully saved.",
              data: updatedRecord
            };
          }
          res.json(jsonResponse);
        });
      });
  });

  // show one record
  app.get("/plates/:recordId", getRecordById, (req, res) => {
    let jsonResponse = {
      message: res.locals.message,
      data: res.locals.data,
      children: res.locals.children
    };
    res.json(jsonResponse);
  });

  // list all records
  app.get("/plates", getAllRecords, (req, res) => {
    let jsonResponse = {
      message: res.locals.message,
      data: res.locals.data
    };
    res.json(jsonResponse);
  });
};

function getAllRecords(req, res, next) {
  Plate.find({}, {}, { sort: { name: 1 } })
    .populate("parentId")
    .exec((error, data) => {
      if (error) {
        res.locals.message =
          "There was a problem with retrieving the records.";
      } else {
        res.locals.message =
          "The records were successfully retrieved.";
      }
      res.locals.data = data;
      return next();
    });
}

function getRecordById(req, res, next) {
	Plate
  .findOne({'_id': req.params.recordId})
  .populate("parentId")
	.exec((error, data) => {
		if(error) {
      res.locals.message = "There was a problem with retrieving the record.";
      res.locals.data = {};
		} else {
			Well
			.find({'parentId': req.params.recordId})
			.exec((error, children) => {
				if(error) {
          res.locals.message = "There was a problem with retrieving the record.";
          res.locals.data = {};
          res.locals.children = [];
				} else {				
					res.locals.message = "The record was successfully retrieved.";
					res.locals.data = data;
					res.locals.children = children;
				}
				return next();
			});		
		}
	});
}
