const User = require('../../../models/User');
const World = require('../../../models/Hardware/Containers/World');
const Freezer = require('../../../models/Hardware/Containers/Freezer');
const jwt = require('jsonwebtoken');
const adminRequired = require("../../../modules/apiAccess").adminRequired;

if (!process.env.JWT_SECRET){ require('../../../config/env.js'); }

module.exports = function(app) {
	
	// create new world
	app.post("/worlds/new", adminRequired, (req, res) => {
    // Instantiate Schema Object
    let newWorld = new World({
			name: req.body.name,
			description: req.body.description
    });
    // Use Mongoose Built In Save Method
		newWorld.save((error, world) => {
			let jsonResponse;
			if(error){
				jsonResponse = {
					message: "There was a problem saving the new World to the DB.",
					world: {}
				};
			} else {
				jsonResponse = {
					message: "The new World was successfully saved to the DB.",
					world: world
				};
			}
			res.json(jsonResponse);
		});
	});	

	// remove world
	app.post("/worlds/:worldId/remove", adminRequired, (req, res) => {
		World
		.findByIdAndRemove(req.params.worldId)
		.exec((error) => {
			if(error){
				jsonResponse = {
					message: "There was a problem removing the World from the DB."
				};
			} else {
				jsonResponse = {
					message: "The World was successfully removed from the DB."
				};
			}
			res.json(jsonResponse);
		});
	});	

	// edit world
	app.post("/worlds/:worldId/edit", adminRequired, (req, res) => {
		World
		.findOne({'_id': req.params.worldId})
		.exec((err, world) => {
			world.name = req.body.name;
			world.description = req.body.description;
			world.save((error, updatedWorld) => {
				let jsonResponse;
				if(error){
					jsonResponse = {
						message: "There was a problem saving the updated World to the DB.",
						world: world
					};
				} else {
					jsonResponse = {
						message: "The updated World was successfully saved to the DB.",
						world: updatedWorld
					};
				}
				res.json(jsonResponse);
			});
		});
	});	

	// show one world
	app.get("/worlds/:worldId", getWorldById, (req, res) => {
		let jsonResponse = {
			message: res.locals.message,
			world: res.locals.world,
			freezers: res.locals.freezers
		};
		res.json(jsonResponse);
	});	

	// list all worlds
	app.get("/worlds", getAllWorlds, (req, res) => {
		let jsonResponse = {
			message: res.locals.message,
			worlds: res.locals.worlds
		};
		res.json(jsonResponse);
	});	

};

function getAllWorlds(req, res, next) {
	World
	.find({}, {}, {sort: {name: 1}})
	.exec((error, worlds) => {
		if(error) {
			res.locals.message = "There was a problem with retrieving the World records from the DB.";
		} else {
			res.locals.message = "The World records were successfully retrieved from the DB.";
		}
		res.locals.worlds = worlds;
		return next();
	});
}

function getWorldById(req, res, next) {
	World
	.findOne({'_id': req.params.worldId})
	.exec((error, world) => {
		if(error) {
			res.locals.message = "There was a problem with retrieving the World record from the DB.";
		} else {
			Freezer
			.find({'parentId': req.params.worldId})
			.exec((error, freezers) => {
				if(error) {
					res.locals.message = "There was a problem with retrieving the World record from the DB.";
				} else {				
					res.locals.message = "The World record was successfully retrieved from the DB.";
					res.locals.world = world;
					res.locals.freezers = freezers;
				}
				return next();
			});		
		}
	});
}
