const User = require("../../../models/User");
const Plate = require("../../../models/Hardware/Containers/Plate");
const Well = require("../../../models/Hardware/Containers/Well");
const DnaSample = require("../../../models/Wetware/Samples/SampleDna");
const OrganismSample = require("../../../models/Wetware/Samples/SampleOrganism");
const Sample = require("../../../models/Wetware/Samples/Sample");
const jwt = require("jsonwebtoken");
const adminRequired = require("../../../modules/apiAccess").adminRequired;

if (!process.env.JWT_SECRET) {
  require("../../../config/env.js");
}

module.exports = function(app) {
  // create new record
  app.post("/wells/new", adminRequired, (req, res) => {
    let newRecord = new Well({
      parentId: req.body.parentId,
      name: req.body.name,
      description: req.body.description,
      wellType: req.body.wellType,
      volume: req.body.volume
    });
    newRecord.save((error, data) => {
      let jsonResponse;
      if (error) {
        jsonResponse = {
          message: "There was a problem saving the new record.",
          data: {},
          error
        };
      } else {
        jsonResponse = {
          message: "The new record was successfully saved.",
          data: data,
          error: {}
        };
      }
      res.json(jsonResponse);
    });
  });

  // remove record
  app.post("/wells/:recordId/remove", adminRequired, (req, res) => {
    Well.findByIdAndRemove(req.params.recordId).exec(error => {
      if (error) {
        jsonResponse = {
          message: "There was a problem removing the record."
        };
      } else {
        jsonResponse = {
          message: "The record was successfully removed."
        };
      }
      res.json(jsonResponse);
    });
  });

  // edit record
  app.post("/wells/:recordId/edit", adminRequired, (req, res) => {
    if (process.env.NODE_ENV === 'test') {
      Well.findOne({ _id: req.params.recordId })
      .exec((err, record) => {
        record.parentId = req.body.parentId;
        record.name = req.body.name;
        record.description = req.body.description;
        record.volume = req.body.volume;
        record.wellType = req.body.wellType;
        record.updatedAt = new Date();
    
        record.save((error, updatedRecord) => {
          let jsonResponse;
          if (error) {
            jsonResponse = {
              message: "There was a problem saving the updated record.",
              data: record
            };
            res.json(jsonResponse);
          } else {
            jsonResponse = {
              message: "The updated record was successfully saved.",
              data: updatedRecord
            };
            res.json(jsonResponse);
          }
          
        });
      });
    } else {
      Well.findOne({ _id: req.params.recordId })
      .populate("parentId")
      .exec((err, record) => {
        record.name = req.body.name;
        record.description = req.body.description;
        record.volume = req.body.volume;
        record.wellType = req.body.wellType;
        record.updateAt = new Date();
    
        record.save((error, updatedRecord) => {
          let jsonResponse;
          if (error) {
            jsonResponse = {
              message: "There was a problem saving the updated record.",
              data: record
            };
          } else {
            jsonResponse = {
              message: "The updated record was successfully saved.",
              data: updatedRecord
            };
          }
          res.json(jsonResponse);
        });
      });      
    }  
  });

  // show one record
  app.get("/wells/:recordId", getRecordById, (req, res) => {
    let jsonResponse = {
      message: res.locals.message,
      data: res.locals.data,
      dnaSamples: res.locals.dnaSamples,
      organismSamples: res.locals.organismSamples,
      samples: res.locals.samples
    };
    res.json(jsonResponse);
  });

  // list all records
  app.get("/wells", getAllRecords, (req, res) => {
    let jsonResponse = {
      message: res.locals.message,
      data: res.locals.data
    };
    res.json(jsonResponse);
  });
};

function getAllRecords(req, res, next) {
  Well.find({}, {}, { sort: { name: 1 } })
    .populate("parentId")
    .exec((error, data) => {
      if (error) {
        res.locals.message =
          "There was a problem with retrieving the records.";
      } else {
        res.locals.message =
          "The records were successfully retrieved.";
      }
      res.locals.data = data;
      return next();
    });
}

function getRecordById(req, res, next) {
	Well
  .findOne({'_id': req.params.recordId})
	.exec((error, data) => {
		if(error) {
      res.locals.message = "There was a problem with retrieving the record.";
      res.locals.data = {};
      return next();
		} else {
			DnaSample
			.find({'parentId': req.params.recordId})
			.exec((error, dnaSamples) => {
				if(error) {
          res.locals.message = "There was a problem with retrieving the record.";
          res.locals.data = {};
          res.locals.dnaSamples = [];
          res.locals.organismSamples = [];
          res.locals.samples = [];
          return next();
				} else {				
          OrganismSample
          .find({'parentId': req.params.recordId})
          .exec((error, organismSamples) => {
            if(error) {
              res.locals.message = "There was a problem with retrieving the record.";
              res.locals.data = {};
              res.locals.dnaSamples = [];
              res.locals.organismSamples = [];
              res.locals.samples = [];
              return next();
            } else {
              Sample
              .find({'parentId': req.params.recordId})
              .exec((error, samples) => {
                if(error) {
                  res.locals.message = "There was a problem with retrieving the record.";
                  res.locals.data = {};
                  res.locals.dnaSamples = [];
                  res.locals.organismSamples = [];
                  res.locals.samples = [];
                } else {
                  res.locals.message = "The record was successfully retrieved.";
                  res.locals.data = data;
                  res.locals.dnaSamples = dnaSamples;
                  res.locals.organismSamples = organismSamples;
                  res.locals.samples = samples;                  
                }
                return next();
              });              
            }
          });
				}
			});		
		}
	});
}
