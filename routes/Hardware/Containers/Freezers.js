const User = require("../../../models/User");
const World = require("../../../models/Hardware/Containers/World");
const Freezer = require("../../../models/Hardware/Containers/Freezer");
const Plate = require("../../../models/Hardware/Containers/Plate");
const jwt = require("jsonwebtoken");
const adminRequired = require("../../../modules/apiAccess").adminRequired;

if (!process.env.JWT_SECRET) {
  require("../../../config/env.js");
}



module.exports = function(app) {
  // create new record
  app.post("/freezers/new", (req, res) => {
    let newRecord = new Freezer({
      parentId: req.body.parentId,
      name: req.body.name,
      description: req.body.description,
      temperature: req.body.temperature,
      // potentialLocation: req.body.potentialLocation,
      // parentLocation: req.body.parentLocation
    });
    newRecord.save((error, data) => {
      let jsonResponse;
      if (error) {
        jsonResponse = {
          message: "There was a problem saving the new record.",
          data: {},
          error
        };
      } else {
        jsonResponse = {
          message: "The new record was successfully saved.",
          data: data,
          error: {}
        };
      }
      res.json(jsonResponse);
    });
  });

  // remove record
  app.post("/freezers/:recordId/remove", adminRequired, (req, res) => {
    Freezer.findByIdAndRemove(req.params.recordId).exec(error => {
      if (error) {
        jsonResponse = {
          message: "There was a problem removing the record."
        };
      } else {
        jsonResponse = {
          message: "The record was successfully removed."
        };
      }
      res.json(jsonResponse);
    });
  });

  // edit record
  app.post("/freezers/:recordId/edit", adminRequired, (req, res) => {
    if (process.env.NODE_ENV === 'test') {
      Freezer.findOne({ _id: req.params.recordId })
      .exec((err, record) => {
        record.name = req.body.name;
        record.description = req.body.description;
        record.temperature = req.body.temperature;
        record.potentialLocation = req.body.potentialLocation;
        record.parentLocation = req.body.parentLocation;
        record.updateAt = new Date();
        record.save((error, updatedRecord) => {
          let jsonResponse;
          if (error) {
            jsonResponse = {
              message: "There was a problem saving the updated record.",
              data: record
            };
          } else {
            jsonResponse = {
              message: "The updated record was successfully saved.",
              data: updatedRecord
            };
          }
          res.json(jsonResponse);
        });
      });
    } else {
      Freezer.findOne({ _id: req.params.recordId })
      .populate("parentId")
      .exec((err, record) => {
        record.name = req.body.name;
        record.description = req.body.description;
        record.temperature = req.body.temperature;
        record.potentialLocation = req.body.potentialLocation;
        record.parentLocation = req.body.parentLocation;
        record.updateAt = new Date();
        record.save((error, updatedRecord) => {
          let jsonResponse;
          if (error) {
            jsonResponse = {
              message: "There was a problem saving the updated record.",
              data: record
            };
          } else {
            jsonResponse = {
              message: "The updated record was successfully saved.",
              data: updatedRecord
            };
          }
          res.json(jsonResponse);
        });
      });    
    }  
  });

  // show one record
  app.get("/freezers/:recordId", getRecordById, (req, res) => {
    let jsonResponse = {
      message: res.locals.message,
      data: res.locals.data,
      children: res.locals.children
    };
    res.json(jsonResponse);
  });

  // list all records
  app.get("/freezers", getAllRecords, (req, res) => {
    let jsonResponse = {
      message: res.locals.message,
      data: res.locals.data
    };
    res.json(jsonResponse);
  });
};

function getAllRecords(req, res, next) {
  if (process.env.NODE_ENV === 'test') {
    Freezer.find({}, {}, { sort: { name: 1 } })
    .exec((error, data) => {
      if (error) {
        res.locals.message = "There was a problem with retrieving the records.";
        res.locals.data = [];
      } else {
        res.locals.message = "The records were successfully retrieved.";
        res.locals.data = data;
      }
      return next();
    });
  } else {
    Freezer.find({}, {}, { sort: { name: 1 } })
    .populate("parentId")
    .exec((error, data) => {
      if (error) {
        res.locals.message = "There was a problem with retrieving the records.";
        res.locals.data = [];
      } else {
        res.locals.message = "The records were successfully retrieved.";
        res.locals.data = data;
      }
      return next();
    });    
  }  
}

function getRecordById(req, res, next) {
  if (process.env.NODE_ENV === 'test') {
    Freezer
    .findOne({'_id': req.params.recordId})
    .exec((error, data) => {
      if(error) {
        res.locals.message = "There was a problem with retrieving the record foo.";
        res.locals.error = error;
        res.locals.data = {};
        return next();
      } else {
        Plate
        .find({'parentId': req.params.recordId})
        .exec((error, children) => {
          if(error) {
            res.locals.message = "There was a problem with retrieving the children records.";
            res.locals.data = {};
            res.locals.children = [];
          } else {				
            res.locals.message = "The record was successfully retrieved.";
            res.locals.data = data;
            res.locals.children = children;
          }
          return next();
        });		
      }
    });
  } else {
    Freezer
    .findOne({'_id': req.params.recordId})
    .populate("parentId")
    .exec((error, data) => {
      if(error) {
        res.locals.message = "There was a problem with retrieving the record foo.";
        res.locals.error = error;
        res.locals.data = {};
        return next();
      } else {
        Plate
        .find({'parentId': req.params.recordId})
        .exec((error, children) => {
          if(error) {
            res.locals.message = "There was a problem with retrieving the children records.";
            res.locals.data = {};
            res.locals.children = [];
          } else {				
            res.locals.message = "The record was successfully retrieved.";
            res.locals.data = data;
            res.locals.children = children;
          }
          return next();
        });		
      }
    });
  }
}
