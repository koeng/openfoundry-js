const User = require("../../../models/User");
const MaterialRequest = require("../../../models/Action/Requests/RequestMaterial");
const jwt = require("jsonwebtoken");
const adminRequired = require("../../../modules/apiAccess").adminRequired;

if (!process.env.JWT_SECRET) {
  require("../../../config/env.js");
}

module.exports = function(app) {
  // create new record
  app.post("/material-requests/new", (req, res) => {
    let newRecord = new MaterialRequest({
      name: req.body.name,
      requesterAffiliation: req.body.requesterAffiliation,
      requesterEmail: req.body.requesterEmail,
      requesterName: req.body.requesterName,
      requesterOrcid: req.body.requesterOrcid,
      shippingAddress: req.body.shippingAddress,
      virtual: req.body.virtual
    });
    newRecord.save((error, data) => {
      let jsonResponse;
      if (error) {
        jsonResponse = {
          message: "There was a problem saving the new record.",
          data: {},
          error
        };
      } else {
        jsonResponse = {
          message: "The new record was successfully saved.",
          data: data,
          error: {}
        };
      }
      res.json(jsonResponse);
    });
  });

  // remove record
  app.post("/material-requests/:recordId/remove", adminRequired, (req, res) => {
    MaterialRequest.findByIdAndRemove(req.params.recordId).exec(error => {
      if (error) {
        jsonResponse = {
          message: "There was a problem removing the record."
        };
      } else {
        jsonResponse = {
          message: "The record was successfully removed."
        };
      }
      res.json(jsonResponse);
    });
  });

  // edit record
  app.post("/material-requests/:recordId/edit", adminRequired, (req, res) => {
    MaterialRequest.findOne({ _id: req.params.recordId })
    .exec((err, record) => {
      record.name = req.body.name;
      record.status = req.body.status;
      record.requesterAffiliation = req.body.requesterAffiliation;
      record.requesterEmail = req.body.requesterEmail;
      record.requesterName = req.body.requesterName;
      record.requesterOrcid = req.body.requesterOrcid;
      record.shippingAddress = req.body.shippingAddress;        
      record.updatedAt = new Date();
      record.save((error, updatedRecord) => {
        let jsonResponse;
        if (error) {
          jsonResponse = {
            message: "There was a problem saving the updated record.",
            data: record
          };
        } else {
          jsonResponse = {
            message: "The updated record was successfully saved.",
            data: updatedRecord
          };
        }
        res.json(jsonResponse);
      });
    });
  });

  // show one record
  app.get("/material-requests/:recordId", getRecordById, (req, res) => {
    let jsonResponse = {
      message: res.locals.message,
      data: res.locals.data
    };
    res.json(jsonResponse);
  });

  // list all records
  app.get("/material-requests", getAllRecords, (req, res) => {
    let jsonResponse = {
      message: res.locals.message,
      data: res.locals.data
    };
    res.json(jsonResponse);
  });
};

function getAllRecords(req, res, next) {
  MaterialRequest.find({}, {}, { sort: { name: 1 } })
  .exec((error, data) => {
    if (error) {
      res.locals.message = "There was a problem with retrieving the records.";
      res.locals.data = [];
    } else {
      res.locals.message = "The records were successfully retrieved.";
      res.locals.data = data;
    }
    return next();
  });
}

function getRecordById(req, res, next) {
  
  MaterialRequest
  .findOne({'_id': req.params.recordId})
  .exec((error, data) => {
    if(error) {
      res.locals.message = "There was a problem with retrieving the record foo.";
      res.locals.data = {};
      res.locals.error = error;
    } else {
      res.locals.message = "The record was successfully retrieved.";
      res.locals.data = data;
      res.locals.error = {};
    }
    return next();
  });

}
