# OpenFoundry

Welcome to the OpenFoundry project, a project of the BioBricks Foundation.

## Goals
The OpenFoundry is a project that aims to automate common lab procedures the right way. The end goal of the OpenFoundry is to enable design, building, testing, and learning in synthetic biology in a decentralized and distributed manner in order to create the infastructure necessary to gain operational master of living matter. 


