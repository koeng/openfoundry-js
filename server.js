const express = require("express");
const mongoose = require("mongoose");
const bodyParser = require("body-parser");
const passport = require("passport");
const bearerToken = require("express-bearer-token");
const jwt = require("jsonwebtoken");

// load the config file if no database unique resource identifier is present
if (!process.env.DB_URI) {
  require("./config/env.js");
}

// set port to the environment variable settings or 3000 if none exist
const port = process.env.PORT || 3000;

// connect to db using environment variables
let databaseConnectionString = `mongodb://${process.env.DB_USERNAME}:${
  process.env.DB_PASSWORD
}@`;

if (process.env.NODE_ENV === "test") {
  databaseConnectionString += `${process.env.DB_TEST_URI}`;
} else {
  databaseConnectionString += `${process.env.DB_URI}`;
}

mongoose.Promise = global.Promise;

const dbOptions = {
  server: { socketOptions: { keepAlive: 1, connectTimeoutMS: 30000 } }
};

mongoose.connect(
  databaseConnectionString,
  dbOptions
);

// instantiate express app
const app = express();

// middleware that accepts urlencoded form data
// https://github.com/expressjs/body-parser#bodyparserurlencodedoptions
app.use(bodyParser.urlencoded({ extended: true }));

// middleware that accepts json
// https://github.com/expressjs/body-parser#bodyparserjsonoptions
app.use(bodyParser.json());

// extracts bearer token from request
// https://github.com/tkellen/js-express-bearer-token
app.use(bearerToken());

// instantiate passport
app.use(passport.initialize());

// set passport login and signup strategies
const localSignupStrategy = require("./passport/local-signup");
const localLoginStrategy = require("./passport/local-login");
passport.use("local-signup", localSignupStrategy);
passport.use("local-login", localLoginStrategy);

// set cross origin resource sharing (CORS) policy
app.use((req, res, next) => {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Credentials", "true");
  res.header(
    "Access-Control-Allow-Methods",
    "GET, HEAD, OPTIONS, POST, PUT, DELETE"
  );
  res.header(
    "Access-Control-Allow-Headers",
    "Access-Control-Allow-Headers, Authorization, Origin, Accept, X-Requested-With, Content-Type, X-Access-Token"
  );
  res.header("Cache-Control", "no-cache");
  next();
});

// static info routes
require("./routes/static.js")(app);

// authentication routes
require("./routes/auth.js")(app, passport);

// api routes
require("./routes/api.js")(app);

// user routes
require("./routes/users.js")(app, passport);

// world routes
require("./routes/Hardware/Containers/Worlds.js")(app);

// lab routes
require("./routes/Hardware/Containers/Labs.js")(app);

// freezer routes
require("./routes/Hardware/Containers/Freezers.js")(app);

// plate routes
require("./routes/Hardware/Containers/Plates.js")(app);

// well routes
require("./routes/Hardware/Containers/Wells.js")(app);

// dna sample routes
require("./routes/Wetware/Samples/SampleDnas.js")(app);

// organism sample routes
require("./routes/Wetware/Samples/SampleOrganisms.js")(app);

// sample routes
require("./routes/Wetware/Samples/Samples.js")(app);

// material request routes
require("./routes/Action/Requests/RequestMaterials.js")(app);

// container routes
require("./routes/Hardware/Containers/Containers.js")(app);

// virtual routes
require("./routes/Wetware/Virtuals/Virtuals.js")(app);

// virtualpart routes
require("./routes/Wetware/Virtuals/VirtualParts.js")(app);

// virtualorganisms routes
require("./routes/Wetware/Virtuals/VirtualOrganisms.js")(app);

// virtualplasmids routes
require("./routes/Wetware/Virtuals/VirtualPlasmids.js")(app);

// virtualsamples routes
require("./routes/Wetware/Virtuals/VirtualSamples.js")(app);

// virtualvectors routes
require("./routes/Wetware/Virtuals/VirtualVectors.js")(app);

// physical routes
require("./routes/Wetware/Samples/Physicals.js")(app);

// files routes
require("./routes/file")(app);

// launch server
app.listen(port, () => {
  console.log(`API running on port ${port}`);
});

module.exports = app;
