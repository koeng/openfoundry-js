//During the test the env variable is set to test
process.env.NODE_ENV = 'test';

let mongoose = require("mongoose");
let Plate = require('../../../models/Hardware/Containers/Plate');

//Require the dev-dependencies
let chai = require('chai');
let chaiHttp = require('chai-http');
let server = require('../../../server');
let should = chai.should();


chai.use(chaiHttp);

describe('Plates', () => {

  beforeEach((done) => {
    Plate.remove({}, (err) => { 
      done();           
    });        
  });

  describe('/GET /plates', () => {
    it('it should GET all the plates', (done) => {
      chai.request(server)
        .get('/plates')
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.a('object');
          res.body.should.have.property('message');
          res.body.should.have.property('data');
          res.body.message.should.be.a('string');
          res.body.data.should.be.a('array');
          res.body.data.length.should.be.eql(0);
          done();
        });
    });
  });

  describe('/POST /plates/new', () => {
    it('it should not POST a plate without parentId field', (done) => {
      let plate = {
        name: "foo",
        description: "bar baz quay",
        plateType: "standard plate"
      };
      chai.request(server)
        .post('/plates/new')
        .send(plate)
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.a('object');
          res.body.should.have.property('error');
          res.body.error.should.have.property('errors');
          res.body.error.errors.should.have.property('parentId');
          res.body.error.errors.parentId.should.have.property('kind').eql('required');
          done();
        });
    });
  });

  describe('/POST /plates/new', () => {
    it('it should not POST a plate without name field', (done) => {
      let plate = {
        parentId: "123",
        description: "bar baz quay",
        plateType: "standard plate"
      };
      chai.request(server)
        .post('/plates/new')
        .send(plate)
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.a('object');
          res.body.should.have.property('error');
          res.body.error.should.have.property('errors');
          res.body.error.errors.should.have.property('name');
          res.body.error.errors.name.should.have.property('kind').eql('required');
          done();
        });
    });
  });

  describe('/POST /plates/new', () => {
    it('it should not POST a plate without plate type field', (done) => {
      let plate = {
        parentId: "123",
        name: "foo",
        description: "bar baz quay"
      };
      chai.request(server)
        .post('/plates/new')
        .send(plate)
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.a('object');
          res.body.should.have.property('error');
          res.body.error.should.have.property('errors');
          res.body.error.errors.should.have.property('plateType');
          res.body.error.errors.plateType.should.have.property('kind').eql('required');
          done();
        });
    });
  });

  describe('/POST /plates/new', () => {
    it('it should successfully POST a plate', (done) => {
      let plate = {
        parentId: "123",
        name: "foo",
        description: "bar baz quay",
        plateType: "standard plate"
      };
      chai.request(server)
        .post('/plates/new')
        .send(plate)
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.a('object');
          res.body.should.have.property('message');
          res.body.should.have.property('data');
          res.body.data.should.be.a('object');
          res.body.data.should.have.property('parentId');
          res.body.data.should.have.property('name');
          res.body.data.should.have.property('description');
          res.body.data.should.have.property('plateType');
          done();
        });
    });
  });

});
