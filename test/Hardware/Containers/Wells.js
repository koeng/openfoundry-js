//During the test the env variable is set to test
process.env.NODE_ENV = 'test';

let mongoose = require("mongoose");
let Well = require('../../../models/Hardware/Containers/Well');

//Require the dev-dependencies
let chai = require('chai');
let chaiHttp = require('chai-http');
let server = require('../../../server');
let should = chai.should();


chai.use(chaiHttp);

describe('Wells', () => {

  beforeEach((done) => {
    Well.remove({}, (err) => { 
      done();           
    });        
  });

  describe('/GET /wells', () => {
    it('it should GET all the wells', (done) => {
      chai.request(server)
        .get('/wells')
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.a('object');
          res.body.should.have.property('message');
          res.body.should.have.property('data');
          res.body.message.should.be.a('string');
          res.body.data.should.be.a('array');
          res.body.data.length.should.be.eql(0);
          done();
        });
    });
  });

  describe('/POST /wells/new', () => {
    it('it should not POST a well without parentId field', (done) => {
      let well = {
        name: "foo",
        description: "bar baz quay",
        wellType: "standard well"
      };
      chai.request(server)
        .post('/wells/new')
        .send(well)
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.a('object');
          res.body.should.have.property('error');
          res.body.error.should.have.property('errors');
          res.body.error.errors.should.have.property('parentId');
          res.body.error.errors.parentId.should.have.property('kind').eql('required');
          done();
        });
    });
    it('it should not POST a well without name field', (done) => {
      let well = {
        parentId: "123",
        description: "bar baz quay",
        wellType: "standard well"
      };
      chai.request(server)
        .post('/wells/new')
        .send(well)
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.a('object');
          res.body.should.have.property('error');
          res.body.error.should.have.property('errors');
          res.body.error.errors.should.have.property('name');
          res.body.error.errors.name.should.have.property('kind').eql('required');
          done();
        });
    });
    it('it should not POST a well without well type field', (done) => {
      let well = {
        parentId: "123",
        name: "foo",
        description: "bar baz quay"
      };
      chai.request(server)
        .post('/wells/new')
        .send(well)
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.a('object');
          res.body.should.have.property('error');
          res.body.error.should.have.property('errors');
          res.body.error.errors.should.have.property('wellType');
          res.body.error.errors.wellType.should.have.property('kind').eql('required');
          done();
        });
    });
    it('it should successfully POST a well', (done) => {
      let well = {
        parentId: "123",
        name: "foo",
        description: "bar baz quay",
        wellType: "standard well"
      };
      chai.request(server)
        .post('/wells/new')
        .send(well)
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.a('object');
          res.body.should.have.property('message');
          res.body.should.have.property('data');
          res.body.data.should.be.a('object');
          res.body.data.should.have.property('parentId');
          res.body.data.should.have.property('name');
          res.body.data.should.have.property('description');
          res.body.data.should.have.property('wellType');
          done();
        });
    });            
  });

  describe('/GET /wells/:recordId', () => {
    it('it should GET a well by id', (done) => {
      let well = new Well({
        parentId: "123",
        name: "foo",
        description: "bar baz quay",
        wellType: "standard well"
      });
      well.save((error, well) => {
        if (error) { console.log("error::", error)}
        let route = `/wells/${well._id}`;
        chai.request(server)
          .get(route)
          .send(well)
          .end((err, res) => {
            if (err) { console.log(err) }
            res.should.have.status(200);
            res.body.should.be.a('object');
            res.body.should.have.property('message');
            res.body.should.have.property('data');
            res.body.data.should.be.a('object');
            res.body.data.should.have.property('parentId');
            res.body.data.should.have.property('name');
            res.body.data.should.have.property('description');
            res.body.data.should.have.property('wellType');
            done();
          });
      });
    });
  });

  describe('/POST /wells/:recordId/edit', () => {
    it('it should UPDATE well by id', (done) => {
      let well = new Well({
        parentId: "123",
        name: "foo",
        description: "bar baz quay",
        wellType: "standard well"
      });
      well.save((error, well) => {
        let route = `/wells/${well._id}/edit`;
        chai.request(server)
          .post(route)
          .send({
            parentId: "123",
            name: "foo",
            description: "bar baz quay",
            wellType: "deep well"        
          })
          .end((err, res) => {
            if (err) { console.log(err) }
            res.should.have.status(200);
            res.body.should.be.a('object');
            res.body.should.have.property('message');
            res.body.should.have.property('data');
            res.body.data.should.be.a('object');
            res.body.data.should.have.property('parentId');
            res.body.data.should.have.property('name');
            res.body.data.should.have.property('description');
            res.body.data.should.have.property('wellType').eql('deep well');
            done();
          });
      });
    });
  });

  describe('/POST /wells/:recordId/remove', () => {
    it('it should DELETE well by id', (done) => {
      let well = new Well({
        parentId: "123",
        name: "foo",
        description: "bar baz quay",
        wellType: "standard well"
      });
      well.save((error, well) => {
        let route = `/wells/${well._id}/remove`;
        chai.request(server)
          .post(route)
          .end((err, res) => {
            if (err) { console.log(err) }
            res.should.have.status(200);
            res.body.should.be.a('object');
            res.body.should.have.property('message').eql('The record was successfully removed.');
            done();
          });
      });
    });
  });

});
