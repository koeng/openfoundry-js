process.env.NODE_ENV = 'test';

let mongoose = require("mongoose");
let Freezer = require('../../../models/Hardware/Containers/Freezer');

let chai = require('chai');
let chaiHttp = require('chai-http');
let server = require('../../../server');
let should = chai.should();


chai.use(chaiHttp);

describe('Freezers', () => {

  beforeEach((done) => {
    Freezer.remove({}, (err) => { 
      done();           
    });        
  });

  describe('/GET /freezers', () => {
    it('it should GET all the freezers', (done) => {
      chai.request(server)
        .get('/freezers')
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.a('object');
          res.body.should.have.property('message');
          res.body.should.have.property('data');
          res.body.message.should.be.a('string');
          res.body.data.should.be.a('array');
          res.body.data.length.should.be.eql(0);
          done();
        });
    });
  });

  describe('/POST /freezers/new', () => {
    it('it should not POST a freezer without parentId field', (done) => {
      let freezer = {
        name: "foo",
        description: "bar baz quay",
        temperature: -80
      };
      chai.request(server)
        .post('/freezers/new')
        .send(freezer)
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.a('object');
          res.body.should.have.property('error');
          res.body.error.should.have.property('errors');
          res.body.error.errors.should.have.property('parentId');
          res.body.error.errors.parentId.should.have.property('kind').eql('required');
          done();
        });
    });
    it('it should not POST a freezer without name field', (done) => {
      let freezer = {
        parentId: "123",
        description: "bar baz quay",
        temperature: -80
      };
      chai.request(server)
        .post('/freezers/new')
        .send(freezer)
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.a('object');
          res.body.should.have.property('error');
          res.body.error.should.have.property('errors');
          res.body.error.errors.should.have.property('name');
          res.body.error.errors.name.should.have.property('kind').eql('required');
          done();
        });
    });
    it('it should not POST a freezer without temperature field', (done) => {
      let freezer = {
        parentId: "123",
        name: "foo",
        description: "bar baz quay"
      };
      chai.request(server)
        .post('/freezers/new')
        .send(freezer)
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.a('object');
          res.body.should.have.property('error');
          res.body.error.should.have.property('errors');
          res.body.error.errors.should.have.property('temperature');
          res.body.error.errors.temperature.should.have.property('kind').eql('required');
          done();
        });
    });
    it('it should successfully POST a freezer', (done) => {
      let freezer = {
        parentId: "123",
        name: "foo",
        description: "bar baz quay",
        temperature: -80
      };
      chai.request(server)
        .post('/freezers/new')
        .send(freezer)
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.a('object');
          res.body.should.have.property('message');
          res.body.should.have.property('data');
          res.body.data.should.be.a('object');
          res.body.data.should.have.property('parentId');
          res.body.data.should.have.property('name');
          res.body.data.should.have.property('description');
          res.body.data.should.have.property('temperature');
          done();
        });
    });            
  });

  describe('/GET /freezers/:recordId', () => {
    it('it should GET a freezer by id', (done) => {
      let freezer = new Freezer({
        parentId: "123",
        name: "foo",
        description: "bar baz quay",
        temperature: -80
      });
      freezer.save((error, freezer) => {
        let route = `/freezers/${freezer._id}`;
        chai.request(server)
          .get(route)
          .send(freezer)
          .end((err, res) => {
            if (err) { console.log(err) }
            res.should.have.status(200);
            res.body.should.be.a('object');
            res.body.should.have.property('message');
            res.body.should.have.property('data');
            res.body.data.should.be.a('object');
            res.body.data.should.have.property('parentId');
            res.body.data.should.have.property('name');
            res.body.data.should.have.property('description');
            res.body.data.should.have.property('temperature');
            done();
          });
      });
    });
  });

  describe('/POST /freezers/:recordId/edit', () => {
    it('it should UPDATE freezer by id', (done) => {
      let freezer = new Freezer({
        parentId: "123",
        name: "foo",
        description: "bar baz quay",
        temperature: -80
      });
      freezer.save((error, freezer) => {
        let route = `/freezers/${freezer._id}/edit`;
        chai.request(server)
          .post(route)
          .send({
            parentId: "123",
            name: "foo",
            description: "bar baz quay",
            temperature: -75            
          })
          .end((err, res) => {
            if (err) { console.log(err) }
            res.should.have.status(200);
            res.body.should.be.a('object');
            res.body.should.have.property('message');
            res.body.should.have.property('data');
            res.body.data.should.be.a('object');
            res.body.data.should.have.property('parentId');
            res.body.data.should.have.property('name');
            res.body.data.should.have.property('description');
            res.body.data.should.have.property('temperature').eql(-75);
            done();
          });
      });
    });
  });

  describe('/POST /freezers/:recordId/remove', () => {
    it('it should DELETE freezer by id', (done) => {
      let freezer = new Freezer({
        parentId: "123",
        name: "foo",
        description: "bar baz quay",
        temperature: -80
      });
      freezer.save((error, freezer) => {
        let route = `/freezers/${freezer._id}/remove`;
        chai.request(server)
          .post(route)
          .end((err, res) => {
            if (err) { console.log(err) }
            res.should.have.status(200);
            res.body.should.be.a('object');
            res.body.should.have.property('message').eql('The record was successfully removed.');
            done();
          });
      });
    });
  });

});
