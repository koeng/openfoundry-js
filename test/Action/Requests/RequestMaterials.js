process.env.NODE_ENV = 'test';

let mongoose = require("mongoose");
let MaterialRequest = require('../../../models/Action/Requests/RequestMaterial');

let chai = require('chai');
let chaiHttp = require('chai-http');
let server = require('../../../server');
let should = chai.should();


chai.use(chaiHttp); 

describe('Material Requests', () => {

  beforeEach((done) => {
    MaterialRequest.remove({}, (err) => { 
      done();           
    });        
  });

  describe('/GET /material-requests', () => {
    it('it should GET all the material requests', (done) => {
      chai.request(server)
        .get('/material-requests')
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.a('object');
          res.body.should.have.property('message');
          res.body.should.have.property('data');
          res.body.message.should.be.a('string');
          res.body.data.should.be.a('array');
          res.body.data.length.should.be.eql(0);
          done();
        });
    });
  });

  describe('/POST /material-requests/new', () => {
    it('it should not POST a material request without name field', (done) => {
      let request = {
        requesterAffiliation: "example affiliation",
        requesterEmail: "foo@example.com",
        requesterName: "Billy Biohacker",
        requesterOrcid: "foobarbaz",
        shippingAddress: "123 Main St. Foobar, CA",
        virtual: {}
      };
      chai.request(server)
        .post('/material-requests/new')
        .send(request)
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.a('object');
          res.body.should.have.property('error');
          res.body.error.should.have.property('errors');
          res.body.error.errors.should.have.property('name');
          res.body.error.errors.name.should.have.property('kind').eql('required');
          done();
        });
    });
    it('it should not POST a material request without requester affiliation field', (done) => {
      let request = {
        name: "Need Foo_1!!!",
        requesterEmail: "foo@example.com",
        requesterName: "Billy Biohacker",
        requesterOrcid: "foobarbaz",
        shippingAddress: "123 Main St. Foobar, CA",
        virtual: {}
      };
      chai.request(server)
        .post('/material-requests/new')
        .send(request)
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.a('object');
          res.body.should.have.property('error');
          res.body.error.should.have.property('errors');
          res.body.error.errors.should.have.property('requesterAffiliation');
          res.body.error.errors.requesterAffiliation.should.have.property('kind').eql('required');
          done();
        });
    });
    it('it should not POST a material request without requester email field', (done) => {
      let request = {
        name: "Need Foo_1!!!",
        requesterAffiliation: "example affiliation",
        requesterName: "Billy Biohacker",
        requesterOrcid: "foobarbaz",
        shippingAddress: "123 Main St. Foobar, CA",
        virtual: {}
      };
      chai.request(server)
        .post('/material-requests/new')
        .send(request)
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.a('object');
          res.body.should.have.property('error');
          res.body.error.should.have.property('errors');
          res.body.error.errors.should.have.property('requesterEmail');
          res.body.error.errors.requesterEmail.should.have.property('kind').eql('required');
          done();
        });
    });
    it('it should not POST a material request without requester name field', (done) => {
      let request = {
        name: "Need Foo_1!!!",
        requesterAffiliation: "example affiliation",
        requesterEmail: "foo@example.com",
        requesterOrcid: "foobarbaz",
        shippingAddress: "123 Main St. Foobar, CA",
        virtual: {}
      };
      chai.request(server)
        .post('/material-requests/new')
        .send(request)
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.a('object');
          res.body.should.have.property('error');
          res.body.error.should.have.property('errors');
          res.body.error.errors.should.have.property('requesterName');
          res.body.error.errors.requesterName.should.have.property('kind').eql('required');
          done();
        });
    });
    it('it should not POST a material request without requester orcid field', (done) => {
      let request = {
        name: "Need Foo_1!!!",
        requesterAffiliation: "example affiliation",
        requesterName: "Billy Biohacker",
        requesterEmail: "foo@example.com",
        shippingAddress: "123 Main St. Foobar, CA",
        virtual: {}
      };
      chai.request(server)
        .post('/material-requests/new')
        .send(request)
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.a('object');
          res.body.should.have.property('error');
          res.body.error.should.have.property('errors');
          res.body.error.errors.should.have.property('requesterOrcid');
          res.body.error.errors.requesterOrcid.should.have.property('kind').eql('required');
          done();
        });
    });
    it('it should not POST a material request without requester shipping address field', (done) => {
      let request = {
        name: "Need Foo_1!!!",
        requesterAffiliation: "example affiliation",
        requesterName: "Billy Biohacker",
        requesterEmail: "foo@example.com",
        requesterOrcid: "foobarbaz",
        virtual: {}
      };
      chai.request(server)
        .post('/material-requests/new')
        .send(request)
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.a('object');
          res.body.should.have.property('error');
          res.body.error.should.have.property('errors');
          res.body.error.errors.should.have.property('shippingAddress');
          res.body.error.errors.shippingAddress.should.have.property('kind').eql('required');
          done();
        });
    });
    it('it should successfully POST a material request', (done) => {
      let request = {
        name: "Need Foo_1!!!",
        requesterAffiliation: "example affiliation",
        requesterEmail: "foo@example.com",
        requesterName: "Billy Biohacker",
        requesterOrcid: "foobarbaz",
        shippingAddress: "123 Main St. Foobar, CA",
        virtual: {}
      };
      chai.request(server)
        .post('/material-requests/new')
        .send(request)
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.a('object');
          res.body.should.have.property('message');
          res.body.should.have.property('data');
          res.body.data.should.be.a('object');
          res.body.data.should.have.property('name');
          res.body.data.should.have.property('status');
          res.body.data.should.have.property('requesterAffiliation');
          res.body.data.should.have.property('requesterEmail');
          res.body.data.should.have.property('requesterName');
          res.body.data.should.have.property('requesterOrcid');
          res.body.data.should.have.property('shippingAddress');
          done();
        });
    });            
  });

  describe('/GET /material-requests/:recordId', () => {
    it('it should GET a material request by id', (done) => {
      let request = new MaterialRequest({
        name: "Need Foo_1!!!",
        requesterAffiliation: "example affiliation",
        requesterEmail: "foo@example.com",
        requesterName: "Billy Biohacker",
        requesterOrcid: "foobarbaz",
        shippingAddress: "123 Main St. Foobar, CA",
        virtual: {}
      });
      request.save((error, request) => {
        let route = `/material-requests/${request._id}`;
        chai.request(server)
          .get(route)
          .send(request)
          .end((err, res) => {
            if (err) { console.log(err) }
            res.should.have.status(200);
            res.body.should.be.a('object');
            res.body.should.have.property('message');
            res.body.should.have.property('data');
            res.body.data.should.be.a('object');
            res.body.data.should.have.property('name');
            res.body.data.should.have.property('status');
            res.body.data.should.have.property('requesterAffiliation');
            res.body.data.should.have.property('requesterEmail');
            res.body.data.should.have.property('requesterName');
            res.body.data.should.have.property('requesterOrcid');
            res.body.data.should.have.property('shippingAddress');
            done();
          });
      });
    });
  });

  describe('/POST /material-requests/:recordId/edit', () => {
    it('it should UPDATE material request by id', (done) => {
      let request = new MaterialRequest({
        name: "Need Foo_1!!!",
        requesterAffiliation: "example affiliation",
        requesterEmail: "foo@example.com",
        requesterName: "Billy Biohacker",
        requesterOrcid: "foobarbaz",
        shippingAddress: "123 Main St. Foobar, CA",
        virtual: {}
      });
      request.save((error, request) => {
        let route = `/material-requests/${request._id}/edit`;
        chai.request(server)
          .post(route)
          .send({
            name: "Need Foo_1!!!",
            requesterAffiliation: "example affiliation",
            requesterEmail: "foo@example.com",
            requesterName: "Billy Biohacker",
            requesterOrcid: "foobarbaz",
            shippingAddress: "123 Main St. Foobar, CA",           
            status: "Approved",
            virtual: {}
          })
          .end((err, res) => {
            if (err) { console.log(err) }
            res.should.have.status(200);
            res.body.should.be.a('object');
            res.body.should.have.property('message');
            res.body.should.have.property('data');
            res.body.data.should.be.a('object');
            res.body.data.should.have.property('name');
            res.body.data.should.have.property('status').eql('Approved');
            res.body.data.should.have.property('requesterAffiliation');
            res.body.data.should.have.property('requesterEmail');
            res.body.data.should.have.property('requesterName');
            res.body.data.should.have.property('requesterOrcid');
            res.body.data.should.have.property('shippingAddress');
            done();
          });
      });
    });
  });

  describe('/POST /material-requests/:recordId/remove', () => {
    it('it should DELETE material request by id', (done) => {
      let request = new MaterialRequest({
        name: "Need Foo_1!!!",
        requesterAffiliation: "example affiliation",
        requesterEmail: "foo@example.com",
        requesterName: "Billy Biohacker",
        requesterOrcid: "foobarbaz",
        shippingAddress: "123 Main St. Foobar, CA"
      });
      request.save((error, request) => {
        let route = `/material-requests/${request._id}/remove`;
        chai.request(server)
          .post(route)
          .end((err, res) => {
            if (err) { console.log(err) }
            res.should.have.status(200);
            res.body.should.be.a('object');
            res.body.should.have.property('message').eql('The record was successfully removed.');
            done();
          });
      });
    });
  });

});
