//During the test the env variable is set to test
process.env.NODE_ENV = 'test';

let mongoose = require("mongoose");
let OrganismSample = require('../../../models/Wetware/Samples/SampleOrganism');

//Require the dev-dependencies
let chai = require('chai');
let chaiHttp = require('chai-http');
let server = require('../../../server');
let should = chai.should();


chai.use(chaiHttp);

describe('Organism Samples', () => {

  beforeEach((done) => {
    OrganismSample.remove({}, (err) => { 
      done();           
    });        
  });

  describe('/GET /organism-samples', () => {
    it('it should GET all the organism samples', (done) => {
      chai.request(server)
        .get('/organism-samples')
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.a('object');
          res.body.should.have.property('message');
          res.body.should.have.property('data');
          res.body.message.should.be.a('string');
          res.body.data.should.be.a('array');
          res.body.data.length.should.be.eql(0);
          done();
        });
    });
  });

  describe('/POST /organism-samples/new', () => {
    it('it should not POST an organism sample without parentId field', (done) => {
      let sample = {
        name: "foo",
        description: "bar baz quay",
        provenanceId: "123",
        provenanceJob: "foo job",
        organism: "foo organism"
      };
      chai.request(server)
        .post('/organism-samples/new')
        .send(sample)
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.a('object');
          res.body.should.have.property('error');
          res.body.error.should.have.property('errors');
          res.body.error.errors.should.have.property('parentId');
          res.body.error.errors.parentId.should.have.property('kind').eql('required');
          done();
        });
    });
  });

  describe('/POST /organism-samples/new', () => {
    it('it should not POST an organism sample without name field', (done) => {
      let sample = {
        parentId: "123",
        description: "bar baz quay",
        provenanceId: "123",
        provenanceJob: "foo job",
        organism: "foo organism"
      };
      chai.request(server)
        .post('/organism-samples/new')
        .send(sample)
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.a('object');
          res.body.should.have.property('error');
          res.body.error.should.have.property('errors');
          res.body.error.errors.should.have.property('name');
          res.body.error.errors.name.should.have.property('kind').eql('required');
          done();
        });
    });
  });

  describe('/POST /organism-samples/new', () => {
    it('it should successfully POST an organism sample', (done) => {
      let sample = {
        parentId: "123",
        name: "foo",
        description: "bar baz quay",
        provenanceId: "123",
        provenanceJob: "foo job",
        organism: "foo organism"
      };
      chai.request(server)
        .post('/organism-samples/new')
        .send(sample)
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.a('object');
          res.body.should.have.property('message');
          res.body.should.have.property('data');
          res.body.data.should.be.a('object');
          res.body.data.should.have.property('parentId');
          res.body.data.should.have.property('name');
          res.body.data.should.have.property('description');
          done();
        });
    });
  });

});
