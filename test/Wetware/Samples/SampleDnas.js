//During the test the env variable is set to test
process.env.NODE_ENV = 'test';

let mongoose = require("mongoose");
let DnaSample = require('../../../models/Wetware/Samples/SampleDna');

//Require the dev-dependencies
let chai = require('chai');
let chaiHttp = require('chai-http');
let server = require('../../../server');
let should = chai.should();


chai.use(chaiHttp);

describe('DNA Samples', () => {

  beforeEach((done) => {
    DnaSample.remove({}, (err) => { 
      done();           
    });        
  });

  describe('/GET /dna-samples', () => {
    it('it should GET all the dna samples', (done) => {
      chai.request(server)
        .get('/dna-samples')
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.a('object');
          res.body.should.have.property('message');
          res.body.should.have.property('data');
          res.body.message.should.be.a('string');
          res.body.data.should.be.a('array');
          res.body.data.length.should.be.eql(0);
          done();
        });
    });
  });

  describe('/POST /dna-samples/new', () => {
    it('it should not POST a dna sample without parentId field', (done) => {
      let sample = {
        name: "foo",
        description: "bar baz quay",
        provenanceId: "123",
        provenanceJob: "foo job",
        dna: "agtc"
      };
      chai.request(server)
        .post('/dna-samples/new')
        .send(sample)
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.a('object');
          res.body.should.have.property('error');
          res.body.error.should.have.property('errors');
          res.body.error.errors.should.have.property('parentId');
          res.body.error.errors.parentId.should.have.property('kind').eql('required');
          done();
        });
    });
    it('it should not POST a dna sample without name field', (done) => {
      let sample = {
        parentId: "123",
        description: "bar baz quay",
        provenanceId: "123",
        provenanceJob: "foo job",
        dna: "agtc"        
      };
      chai.request(server)
        .post('/dna-samples/new')
        .send(sample)
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.a('object');
          res.body.should.have.property('error');
          res.body.error.should.have.property('errors');
          res.body.error.errors.should.have.property('name');
          res.body.error.errors.name.should.have.property('kind').eql('required');
          done();
        });
    });
    it('it should not POST a dna sample without provenance id field', (done) => {
      let sample = {
        parentId: "123",
        name: "foo",
        description: "bar baz quay",
        provenanceJob: "foo job",
        dna: "agtc"        
      };
      chai.request(server)
        .post('/dna-samples/new')
        .send(sample)
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.a('object');
          res.body.should.have.property('error');
          res.body.error.should.have.property('errors');
          res.body.error.errors.should.have.property('provenanceId');
          res.body.error.errors.provenanceId.should.have.property('kind').eql('required');
          done();
        });
    });
    it('it should not POST a dna sample without provenance job field', (done) => {
      let sample = {
        parentId: "123",
        name: "foo",
        description: "bar baz quay",
        provenanceId: "123",
        dna: "agtc"        
      };
      chai.request(server)
        .post('/dna-samples/new')
        .send(sample)
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.a('object');
          res.body.should.have.property('error');
          res.body.error.should.have.property('errors');
          res.body.error.errors.should.have.property('provenanceJob');
          res.body.error.errors.provenanceJob.should.have.property('kind').eql('required');
          done();
        });
    });
    it('it should successfully POST a dna sample', (done) => {
      let sample = {
        parentId: "123",
        name: "foo",
        description: "bar baz quay",
        provenanceId: "123",
        provenanceJob: "foo job",
        dna: "agtc"
      };
      chai.request(server)
        .post('/dna-samples/new')
        .send(sample)
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.a('object'); 
          res.body.should.have.property('message');
          res.body.should.have.property('data');
          res.body.data.should.be.a('object');
          res.body.data.should.have.property('parentId');
          res.body.data.should.have.property('name');
          res.body.data.should.have.property('description');
          res.body.data.should.have.property('provenanceId');
          res.body.data.should.have.property('provenanceJob');
          res.body.data.should.have.property('dna');
          done();
        });
    });    
  });

});
