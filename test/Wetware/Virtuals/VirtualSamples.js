//During the test the env variable is set to test
process.env.NODE_ENV = "test";

const mongoose = require("mongoose");
const shortid = require("shortid");
const VirtualSample = require("../../../models/Wetware/Virtuals/VirtualSample");

//Require the dev-dependencies
const chai = require("chai");
const chaiHttp = require("chai-http");
const server = require("../../../server");
const should = chai.should();

chai.use(chaiHttp);

describe("Virtual Samples", () => {
  beforeEach(done => {
    VirtualSample.remove({}, err => {
      done();
    });
  });

  describe("/GET /virtualsamples", () => {
    it("it should GET all the virtualsamples", done => {
      chai
        .request(server)
        .get("/virtualsamples")
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.a("object");
          res.body.should.have.property("message");
          res.body.should.have.property("data");
          res.body.message.should.be.a("string");
          res.body.data.should.be.a("array");
          res.body.data.length.should.be.eql(0);
          done();
        });
    });
  });

  describe("/POST /virtualsamples/new", () => {
    // author
    it("it should not POST a virtual sample without name field", done => {
      let virtualsample = {
        description: "bar baz quay",
        status: "fooford",
        authorAffiliation: "fooford2",
        authorEmail: "foo@bar.baz2",
        authorName: "foobar",
        authorOrcid: "http://orcid/foo",
        datName: "fooDat",
        datHash: "fooHash",
        createdByJob: "blabla",
        derivedFrom: ["bop", "bap"],
        originNode: "burp"
      };
      chai
        .request(server)
        .post("/virtualsamples/new")
        .send(virtualsample)
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.a("object");
          res.body.should.have.property("error");
          res.body.error.should.have.property("errors");
          res.body.error.errors.should.have.property("name");
          res.body.error.errors.name.should.have
            .property("kind")
            .eql("required");
          done();
        });
    });
    // name
    it("it should not POST a virtual sample without description field", done => {
      let virtualsample = {
        name: "bur",
        status: "fooford",
        authorAffiliation: "fooford2",
        authorEmail: "foo@bar.baz2",
        authorName: "foobar",
        authorOrcid: "http://orcid/foo",
        datName: "fooDat",
        datHash: "fooHash",
        createdByJob: "blabla",
        derivedFrom: ["bop", "bap"],
        originNode: "burp"
      };
      chai
        .request(server)
        .post("/virtualsamples/new")
        .send(virtualsample)
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.a("object");
          res.body.should.have.property("error");
          res.body.error.should.have.property("errors");
          res.body.error.errors.should.have.property("description");
          res.body.error.errors.description.should.have
            .property("kind")
            .eql("required");
          done();
        });
    });

    it("it should not POST a virtual sample without author affiliation field", done => {
      let virtualsample = {
        name: "bur",
        description: "bar baz quay",
        status: "fooford",
        authorEmail: "foo@bar.baz2",
        authorName: "foobar",
        authorOrcid: "http://orcid/foo",
        datName: "fooDat",
        datHash: "fooHash",
        createdByJob: "blabla",
        derivedFrom: ["bop", "bap"],
        originNode: "burp"
      };
      chai
        .request(server)
        .post("/virtualsamples/new")
        .send(virtualsample)
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.a("object");
          res.body.should.have.property("error");
          res.body.error.should.have.property("errors");
          res.body.error.errors.should.have.property("authorAffiliation");
          res.body.error.errors.authorAffiliation.should.have
            .property("kind")
            .eql("required");
          done();
        });
    });

    // authorName
    it("it should not POST a virtual sample without author name field", done => {
      let virtualsample = {
        name: "bur",
        description: "bar baz quay",
        status: "fooford",
        authorAffiliation: "fooford2",
        authorEmail: "foo@bar.baz2",
        authorOrcid: "http://orcid/foo",
        datName: "fooDat",
        datHash: "fooHash",
        createdByJob: "blabla",
        derivedFrom: ["bop", "bap"],
        originNode: "burp"
      };
      chai
        .request(server)
        .post("/virtualsamples/new")
        .send(virtualsample)
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.a("object");
          res.body.should.have.property("error");
          res.body.error.should.have.property("errors");
          res.body.error.errors.should.have.property("authorName");
          res.body.error.errors.authorName.should.have
            .property("kind")
            .eql("required");
          done();
        });
    });

    //authorEmail
    it("it should not POST a virtual sample without author orcid field", done => {
      let virtualsample = {
        name: "bur",
        description: "bar baz quay",
        status: "fooford",
        authorAffiliation: "fooford2",
        authorName: "foobar",
        authorEmail: "foo@bar.baz2",
        datName: "fooDat",
        datHash: "fooHash",
        createdByJob: "blabla",
        derivedFrom: ["bop", "bap"],
        originNode: "burp"
      };
      chai
        .request(server)
        .post("/virtualsamples/new")
        .send(virtualsample)
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.a("object");
          res.body.should.have.property("error");
          res.body.error.should.have.property("errors");
          res.body.error.errors.should.have.property("authorOrcid");
          res.body.error.errors.authorOrcid.should.have
            .property("kind")
            .eql("required");
          done();
        });
    });
    it("it should not POST a virtual sample without author email field", done => {
      let virtualsample = {
        name: "bur",
        description: "bar baz quay",
        status: "fooford",
        authorAffiliation: "fooford2",
        authorName: "foobar",
        authorOrcid: "http://orcid/foo",
        datName: "fooDat",
        datHash: "fooHash",
        createdByJob: "blabla",
        derivedFrom: ["bop", "bap"],
        originNode: "burp"
      };
      chai
        .request(server)
        .post("/virtualsamples/new")
        .send(virtualsample)
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.a("object");
          res.body.should.have.property("error");
          res.body.error.should.have.property("errors");
          res.body.error.errors.should.have.property("authorEmail");
          res.body.error.errors.authorEmail.should.have
            .property("kind")
            .eql("required");
          done();
        });
    });
    it("it should not POST a virtual sample without created by job field", done => {
      let virtualsample = {
        name: "bur",
        description: "bar baz quay",
        status: "fooford",
        authorAffiliation: "fooford2",
        authorEmail: "foo@bar.baz2",
        authorName: "foobar",
        authorOrcid: "http://orcid/foo",
        datName: "fooDat",
        datHash: "fooHash",
        derivedFrom: ["bop", "bap"],
        originNode: "burp"
      };
      chai
        .request(server)
        .post("/virtualsamples/new")
        .send(virtualsample)
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.a("object");
          res.body.should.have.property("error");
          res.body.error.should.have.property("errors");
          res.body.error.errors.should.have.property("createdByJob");
          res.body.error.errors.createdByJob.should.have
            .property("kind")
            .eql("required");
          done();
        });
    });

    it("it should not POST a virtual sample without derived from field", done => {
      let virtualsample = {
        name: "bur",
        description: "bar baz quay",
        status: "fooford",
        authorAffiliation: "fooford2",
        authorEmail: "foo@bar.baz2",
        authorName: "foobar",
        authorOrcid: "http://orcid/foo",
        datName: "fooDat",
        datHash: "fooHash",
        createdByJob: "blabla",
        originNode: "burp"
      };
      chai
        .request(server)
        .post("/virtualsamples/new")
        .send(virtualsample)
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.a("object");
          res.body.should.have.property("error");
          res.body.error.should.have.property("errors");
          res.body.error.errors.should.have.property("derivedFrom");
          res.body.error.errors.derivedFrom.should.have
            .property("kind")
            .eql("required");
          done();
        });
    });

    it("it should not POST a virtual sample without origin node field", done => {
      let virtualsample = {
        name: "bur",
        description: "bar baz quay",
        status: "fooford",
        authorAffiliation: "fooford2",
        authorEmail: "foo@bar.baz2",
        authorName: "foobar",
        authorOrcid: "http://orcid/foo",
        datName: "fooDat",
        datHash: "fooHash",
        createdByJob: "blabla",
        derivedFrom: ["bop", "bap"]
      };
      chai
        .request(server)
        .post("/virtualsamples/new")
        .send(virtualsample)
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.a("object");
          res.body.should.have.property("error");
          res.body.error.should.have.property("errors");
          res.body.error.errors.should.have.property("originNode");
          res.body.error.errors.originNode.should.have
            .property("kind")
            .eql("required");
          done();
        });
    });
    it("it should successfully POST a virtual sample", done => {
      let virtualsample = {
        author: "foo",
        name: "bur",
        description: "bar baz quay",
        status: "fooford",
        authorAffiliation: "fooford2",
        authorEmail: "foo@bar.baz2",
        authorName: "foobar",
        authorOrcid: "http://orcid/foo",
        datName: "fooDat",
        datHash: "fooHash",
        createdByJob: "blabla",
        derivedFrom: ["bop", "bap"],
        originNode: "burp"
      };
      chai
        .request(server)
        .post("/virtualsamples/new")
        .send(virtualsample)
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.a("object");
          res.body.should.have.property("message");
          res.body.should.have.property("data");
          res.body.data.should.be.a("object");
          res.body.data.should.have.property("name");
          res.body.data.should.have.property("description");
          res.body.data.should.have.property("authorAffiliation");
          res.body.data.should.have.property("authorEmail");
          res.body.data.should.have.property("authorName");
          res.body.data.should.have.property("authorOrcid");
          res.body.data.should.have.property("datName");
          res.body.data.should.have.property("datHash");
          res.body.data.should.have.property("createdByJob");
          res.body.data.should.have.property("derivedFrom");
          res.body.data.should.have.property("originNode");
          done();
        });
    });

  });
});


describe("/GET /virtualsamples/:recordId", () => {
  it("it should GET a virtualsample by id", done => {
    let virtualsample = new VirtualSample({
      name: "foo",
      description: "bar baz quay",
      status: "fooford",
      authorAffiliation: "fooford2",
      authorEmail: "foo@bar.baz2",
      authorName: "foobar",
      authorOrcid: "http://orcid/foo",
      datName: "fooDat",
      datHash: "fooHash",
      createdByJob: "blabla",
      derivedFrom: ["bop", "bap"],
      originNode: "burp"
    });
    virtualsample.save((error, virtualsample) => {
      if (error) {
        console.log(error);
        done();
      } else {
        let route = `/virtualsamples/${virtualsample._id}`;
        chai
          .request(server)
          .get(route)
          .send(virtualsample)
          .end((err, res) => {
            res.should.have.status(200);
            res.body.should.be.a("object");
            res.body.should.have.property("message");
            res.body.should.have.property("data");
            res.body.data.should.be.a("object");
            res.body.data.should.have.property("name");
            res.body.data.should.have.property("description");
            res.body.data.should.have.property("authorAffiliation");
            res.body.data.should.have.property("authorEmail");
            res.body.data.should.have.property("authorOrcid");
            res.body.data.should.have.property("createdByJob");
            res.body.data.should.have.property("derivedFrom");
            res.body.data.should.have.property("originNode");
            done();
          });
      }
    });
  });
});

describe("/POST /virtualsamples/:recordId/edit", () => {
  it("it should UPDATE virtualsample by id", done => {
    let virtualsample = new VirtualSample({
      name: "foo",
      description: "bar baz quay",
      status: "bar",
      authorAffiliation: "fooford2",
      authorEmail: "foo@bar.baz2",
      authorName: "foobar",
      authorOrcid: "http://orcid/foo",
      datName: "fooDat",
      datHash: "fooHash",
      createdByJob: "fooBeez",
      derivedFrom: ["bop", "bap"],
      originNode: "burp"
    });
    virtualsample.save((error, virtualsample) => {
      if (error) {
        console.log(error);
      }
      let route = `/virtualsamples/${virtualsample._id}/edit`;
      chai
        .request(server)
        .post(route)
        .send({
          name: "foo",
          description: "bar baz quay",
          status: "bar",
          authorAffiliation: "fooford2",
          authorEmail: "foo@bar.baz2",
          authorName: "foobar",
          authorOrcid: "http://orcid/foo",
          datName: "fooDat",
          datHash: "fooHash",
          createdByJob: "fooBeez",
          derivedFrom: ["bop", "bap"],
          originNode: "burp"
        })
        .end((err, res) => {
          if (err) {
            console.log(err);
          }
          res.should.have.status(200);
          res.body.should.be.a("object");
          res.body.should.have.property("message");
          res.body.should.have.property("data");
          res.body.data.should.be.a("object");
          res.body.data.should.have.property("name").eql("foo");
          res.body.data.should.have.property("description").eql("bar baz quay");
          res.body.data.should.have.property("status").eql("bar");
          res.body.data.should.have
            .property("authorAffiliation")
            .eql("fooford2");
          res.body.data.should.have.property("authorEmail").eql("foo@bar.baz2");
          res.body.data.should.have.property("authorName").eql("foobar");
          res.body.data.should.have
            .property("authorOrcid")
            .eql("http://orcid/foo");
          res.body.data.should.have.property("createdByJob").eql("fooBeez");
          res.body.data.should.have.property("datName").eql("fooDat");
          res.body.data.should.have.property("datHash").eql("fooHash");
          res.body.data.should.have.property("derivedFrom").eql(["bop", "bap"]);
          res.body.data.should.have.property("originNode").eql("burp");
          done();
        });
    });
  });
});

describe("/POST /virtualsamples/:recordId/remove", () => {
  it("it should DELETE virtual sample by id", done => {
    let virtualsample = new VirtualSample({
      name: "bur",
      description: "bar baz quay",
      status: "fooford",
      authorAffiliation: "fooford2",
      authorEmail: "foo@bar.baz2",
      authorName: "foobar",
      authorOrcid: "http://orcid/foo",
      datName: "fooDat",
      datHash: "fooHash",
      createdByJob: "fooBeez",
      derivedFrom: ["bop", "bap"],
      originNode: "burp"
    });
    virtualsample.save((error, virtualsample) => {
      let route = `/virtualsamples/${virtualsample._id}/remove`;
      chai
        .request(server)
        .post(route)
        .end((err, res) => {
          if (err) {
            console.log(err);
          }
          res.should.have.status(200);
          res.body.should.be.a("object");
          res.body.should.have
            .property("message")
            .eql("The record was successfully removed.");
          done();
        });
    });
  });
});
