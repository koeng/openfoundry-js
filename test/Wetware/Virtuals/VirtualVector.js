//During the test the env variable is set to test
process.env.NODE_ENV = "test";

const mongoose = require("mongoose");
const shortid = require("shortid");
const VirtualVector = require("../../../models/Wetware/Virtuals/VirtualVector");

//Require the dev-dependencies
const chai = require("chai");
const chaiHttp = require("chai-http");
const server = require("../../../server");
const should = chai.should();

chai.use(chaiHttp);

describe("Virtual Vector", () => {
  beforeEach(done => {
    VirtualVector.remove({}, err => {
      done();
    });
  });

  describe("/GET /virtualvectors", () => {
    it("it should GET all the virtual vectors", done => {
      chai
        .request(server)
        .get("/virtualvectors")
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.a("object");
          res.body.should.have.property("message");
          res.body.should.have.property("data");
          res.body.message.should.be.a("string");
          res.body.data.should.be.a("array");
          res.body.data.length.should.be.eql(0);
          done();
        });
    });
  });

  describe("/POST /virtualvectors/new", () => {
    // author
    it("it should not POST a virtual vector without author affiliation field", done => {
      let virtualVector = {
        authorEmail: "foo@bar.baz2",
        authorName: "foobar",
        authorOrcid: "http://orcid/foo",
        name: "meep",
        description: "bar baz quay",
        status: "fooford",
        targetOrganisms: ["beep", "bap"],
        sequence: "nice",
        temperature: 2,
        positiveSelection: ["foo", "bar", "baz"],
        negativeSelection: ["baz", "bar", "foo"],
        genotypeConditions: ["bar", "baz", "foo"],
        media: ["yaa", "hoo"],
        createdByJob: "derp",
        derivedFrom: ["team", "do"],
        originNode: "meep"
      };
      chai
        .request(server)
        .post("/virtualvectors/new")
        .send(virtualVector)
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.a("object");
          res.body.should.have.property("error");
          res.body.error.should.have.property("errors");
          res.body.error.errors.should.have.property("authorAffiliation");
          res.body.error.errors.authorAffiliation.should.have
            .property("kind")
            .eql("required");
          done();
        });
    });
    // name
    it("it should not POST a virtual vector without Author Email field", done => {
      let virtualVector = {
        authorAffiliation: "fooford2",
        authorName: "foobar",
        authorOrcid: "http://orcid/foo",
        name: "meep",
        description: "bar baz quay",
        status: "fooford",
        targetOrganisms: ["beep", "bap"],
        sequence: "nice",
        temperature: 2,
        positiveSelection: ["foo", "bar", "baz"],
        negativeSelection: ["baz", "bar", "foo"],
        genotypeConditions: ["bar", "baz", "foo"],
        media: ["yaa", "hoo"],
        createdByJob: "derp",
        derivedFrom: ["team", "do"],
        originNode: "meep"
      };
      chai
        .request(server)
        .post("/virtualvectors/new")
        .send(virtualVector)
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.a("object");
          res.body.should.have.property("error");
          res.body.error.should.have.property("errors");
          res.body.error.errors.should.have.property("authorEmail");
          res.body.error.errors.authorEmail.should.have
            .property("kind")
            .eql("required");
          done();
        });
    });
    // author email
    it("it should not POST a virtualVector without author name field", done => {
      let virtualVector = {
        authorAffiliation: "fooford2",
        authorEmail: "foo@bar.baz2",
        authorOrcid: "http://orcid/foo",
        name: "beep",
        description: "bar baz quay",
        status: "fooford",
        targetOrganisms: ["beep", "bap"],
        sequence: "nice",
        temperature: 2,
        positiveSelection: ["foo", "bar", "baz"],
        negativeSelection: ["baz", "bar", "foo"],
        genotypeConditions: ["bar", "baz", "foo"],
        media: ["yaa", "hoo"],
        createdByJob: "derp",
        derivedFrom: ["team", "do"],
        originNode: "meep"
      };
      chai
        .request(server)
        .post("/virtualvectors/new")
        .send(virtualVector)
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.a("object");
          res.body.should.have.property("error");
          res.body.error.should.have.property("errors");
          res.body.error.errors.should.have.property("authorName");
          res.body.error.errors.authorName.should.have
            .property("kind")
            .eql("required");
          done();
        });
    });
    // Author Affiliation
    it("it should not POST a virtual vector without author or cid field", done => {
      let virtualVector = {
        authorAffiliation: "fooford2",
        authorEmail: "foo@bar.baz2",
        authorName: "foobar",
        name: "meep",
        description: "bar baz quay",
        status: "fooford",
        targetOrganisms: ["beep", "bap"],
        sequence: "nice",
        temperature: 2,
        positiveSelection: ["foo", "bar", "baz"],
        negativeSelection: ["baz", "bar", "foo"],
        genotypeConditions: ["bar", "baz", "foo"],
        media: ["yaa", "hoo"],
        createdByJob: "derp",
        derivedFrom: ["team", "do"],
        originNode: "meep"
      };
      chai
        .request(server)
        .post("/virtualvectors/new")
        .send(virtualVector)
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.a("object");
          res.body.should.have.property("error");
          res.body.error.should.have.property("errors");
          res.body.error.errors.should.have.property("authorOrcid");
          res.body.error.errors.authorOrcid.should.have
            .property("kind")
            .eql("required");
          done();
        });
    });
    // author or CID
    it("it should not POST a virtual vector without name field", done => {
      let virtualVector = {
        authorAffiliation: "fooford2",
        authorEmail: "foo@bar.baz2",
        authorName: "foobar",
        authorOrcid: "http://orcid/foo",
        description: "bar baz quay",
        status: "fooford",
        targetOrganisms: ["beep", "bap"],
        sequence: "nice",
        temperature: 2,
        positiveSelection: ["foo", "bar", "baz"],
        negativeSelection: ["baz", "bar", "foo"],
        genotypeConditions: ["bar", "baz", "foo"],
        media: ["yaa", "hoo"],
        createdByJob: "derp",
        derivedFrom: ["team", "do"],
        originNode: "meep"
      };
      chai
        .request(server)
        .post("/virtualvectors/new")
        .send(virtualVector)
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.a("object");
          res.body.should.have.property("error");
          res.body.error.should.have.property("errors");
          res.body.error.errors.should.have.property("name");
          res.body.error.errors.name.should.have
            .property("kind")
            .eql("required");
          done();
        });
    });
    // sequence
    it("it should not POST a virtualVector without description field", done => {
      let virtualVector = {
        authorAffiliation: "fooford2",
        authorEmail: "foo@bar.baz2",
        authorName: "foobar",
        authorOrcid: "http://orcid/foo",
        name: "meep",
        status: "fooford",
        targetOrganisms: ["beep", "bap"],
        sequence: "nice",
        temperature: 2,
        positiveSelection: ["foo", "bar", "baz"],
        negativeSelection: ["baz", "bar", "foo"],
        genotypeConditions: ["bar", "baz", "foo"],
        media: ["yaa", "hoo"],
        createdByJob: "derp",
        derivedFrom: ["team", "do"],
        originNode: "meep"
      };
      chai
        .request(server)
        .post("/virtualvectors/new")
        .send(virtualVector)
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.a("object");
          res.body.should.have.property("error");
          res.body.error.should.have.property("errors");
          res.body.error.errors.should.have.property("description");
          res.body.error.errors.description.should.have
            .property("kind")
            .eql("required");
          done();
        });
    });
    it("it should not POST a virtualVector without sequence field", done => {
      let virtualVector = {
        authorAffiliation: "fooford2",
        authorEmail: "foo@bar.baz2",
        authorName: "foobar",
        authorOrcid: "http://orcid/foo",
        name: "meep",
        status: "fooford",
        targetOrganisms: ["beep", "bap"],
        temperature: 2,
        positiveSelection: ["foo", "bar", "baz"],
        negativeSelection: ["baz", "bar", "foo"],
        genotypeConditions: ["bar", "baz", "foo"],
        media: ["yaa", "hoo"],
        createdByJob: "derp",
        derivedFrom: ["team", "do"],
        originNode: "meep"
      };
      chai
        .request(server)
        .post("/virtualvectors/new")
        .send(virtualVector)
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.a("object");
          res.body.should.have.property("error");
          res.body.error.should.have.property("errors");
          res.body.error.errors.should.have.property("sequence");
          res.body.error.errors.sequence.should.have
            .property("kind")
            .eql("required");
          done();
        });
    });

    it("it should not POST a virtualVector without created by job field", done => {
      let virtualVector = {
        authorAffiliation: "fooford2",
        authorEmail: "foo@bar.baz2",
        authorName: "foobar",
        authorOrcid: "http://orcid/foo",
        name: "meep",
        status: "fooford",
        targetOrganisms: ["beep", "bap"],
        temperature: 2,
        positiveSelection: ["foo", "bar", "baz"],
        negativeSelection: ["baz", "bar", "foo"],
        genotypeConditions: ["bar", "baz", "foo"],
        media: ["yaa", "hoo"],
        derivedFrom: ["team", "do"],
        originNode: "meep"
      };
      chai
        .request(server)
        .post("/virtualvectors/new")
        .send(virtualVector)
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.a("object");
          res.body.should.have.property("error");
          res.body.error.should.have.property("errors");
          res.body.error.errors.should.have.property("createdByJob");
          res.body.error.errors.createdByJob.should.have
            .property("kind")
            .eql("required");
          done();
        });
    });

    it("it should not POST a virtualVector without derived from field", done => {
      let virtualVector = {
        authorAffiliation: "fooford2",
        authorEmail: "foo@bar.baz2",
        authorName: "foobar",
        authorOrcid: "http://orcid/foo",
        name: "meep",
        status: "fooford",
        targetOrganisms: ["beep", "bap"],
        temperature: 2,
        positiveSelection: ["foo", "bar", "baz"],
        negativeSelection: ["baz", "bar", "foo"],
        genotypeConditions: ["bar", "baz", "foo"],
        media: ["yaa", "hoo"],
        originNode: "meep"
      };
      chai
        .request(server)
        .post("/virtualvectors/new")
        .send(virtualVector)
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.a("object");
          res.body.should.have.property("error");
          res.body.error.should.have.property("errors");
          res.body.error.errors.should.have.property("derivedFrom");
          res.body.error.errors.derivedFrom.should.have
            .property("kind")
            .eql("required");
          done();
        });
    });

    it("it should not POST a virtualVector without origin node field", done => {
      let virtualVector = {
        authorAffiliation: "fooford2",
        authorEmail: "foo@bar.baz2",
        authorName: "foobar",
        authorOrcid: "http://orcid/foo",
        name: "meep",
        status: "fooford",
        targetOrganisms: ["beep", "bap"],
        temperature: 2,
        positiveSelection: ["foo", "bar", "baz"],
        negativeSelection: ["baz", "bar", "foo"],
        genotypeConditions: ["bar", "baz", "foo"],
        media: ["yaa", "hoo"]
      };
      chai
        .request(server)
        .post("/virtualvectors/new")
        .send(virtualVector)
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.a("object");
          res.body.should.have.property("error");
          res.body.error.should.have.property("errors");
          res.body.error.errors.should.have.property("originNode");
          res.body.error.errors.originNode.should.have
            .property("kind")
            .eql("required");
          done();
        });
    });

    it("it should successfully POST a virtual vector", done => {
      let virtualVector = {
        authorAffiliation: "fooford2",
        authorEmail: "foo@bar.baz2",
        authorName: "foobar",
        authorOrcid: "http://orcid/foo",
        name: "meep",
        description: "bar baz quay",
        status: "fooford",
        targetOrganisms: ["beep", "bap"],
        sequence: "nice",
        temperature: 2,
        positiveSelection: ["foo", "bar", "baz"],
        negativeSelection: ["baz", "bar", "foo"],
        genotypeConditions: ["bar", "baz", "foo"],
        media: ["yaa", "hoo"],
        createdByJob: "derp",
        derivedFrom: ["team", "do"],
        originNode: "meep"
      };
      chai
        .request(server)
        .post("/virtualvectors/new")
        .send(virtualVector)
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.a("object");
          res.body.should.have.property("message");
          res.body.should.have.property("data");
          res.body.data.should.be.a("object");
          res.body.data.should.have.property("authorAffiliation");
          res.body.data.should.have.property("authorEmail");
          res.body.data.should.have.property("authorName");
          res.body.data.should.have.property("authorOrcid");
          res.body.data.should.have.property("name");
          res.body.data.should.have.property("description");
          res.body.data.should.have.property("sequence");
          res.body.data.should.have.property("createdByJob");
          res.body.data.should.have.property("derivedFrom");
          res.body.data.should.have.property("originNode");
          done();
        });
    });
  });

  describe("/GET /virtualvectors/:recordId", () => {
    it("it should GET a virtual vector by id", done => {
      let virtualVector = new VirtualVector({
        authorAffiliation: "fooford2",
        authorEmail: "foo@bar.baz2",
        authorName: "foobar",
        authorOrcid: "http://orcid/foo",
        name: "meep",
        description: "bar baz quay",
        status: "fooford",
        targetOrganisms: ["beep", "bap"],
        sequence: "nice",
        temperature: 2,
        positiveSelection: ["foo", "bar", "baz"],
        negativeSelection: ["baz", "bar", "foo"],
        genotypeConditions: ["bar", "baz", "foo"],
        media: ["yaa", "hoo"],
        createdByJob: "derp",
        derivedFrom: ["team", "do"],
        originNode: "meep"
      });
      virtualVector.save((error, virtualVector) => {
        if (error) {
          console.log(error);
          done();
        } else {
          let route = `/virtualvectors/${virtualVector._id}`;
          chai
            .request(server)
            .get(route)
            .send(virtualVector)
            .end((err, res) => {
              res.should.have.status(200);
              res.body.should.be.a("object");
              res.body.should.have.property("message");
              res.body.should.have.property("data");
              res.body.data.should.be.a("object");
              res.body.data.should.have.property("authorAffiliation");
              res.body.data.should.have.property("authorEmail");
              res.body.data.should.have.property("authorName");
              res.body.data.should.have.property("authorOrcid");
              res.body.data.should.have.property("name");
              res.body.data.should.have.property("description");
              res.body.data.should.have.property("sequence");
              res.body.data.should.have.property("createdByJob");
              res.body.data.should.have.property("derivedFrom");
              res.body.data.should.have.property("originNode");
              done();
            });
        }
      });
    });
  });

  describe("/POST /virtualvectors/:recordId/edit", () => {
    it("it should UPDATE virtualVector by id", done => {
      let virtualVector = new VirtualVector({
        authorAffiliation: "fooford2",
        authorEmail: "foo@bar.baz2",
        authorName: "foobar",
        authorOrcid: "http://orcid/foo",
        name: "meep",
        description: "bar baz quay",
        status: "fooford",
        targetOrganisms: ["beep", "bap"],
        sequence: "nice",
        temperature: 2,
        positiveSelection: ["foo", "bar", "baz"],
        negativeSelection: ["baz", "bar", "foo"],
        genotypeConditions: ["bar", "baz", "foo"],
        media: ["yaa", "hoo"],
        createdByJob: "derp",
        derivedFrom: ["team", "do"],
        originNode: "meep"
      });
      virtualVector.save((error, virtualVector) => {
        if (error) {
          console.log(error);
        }
        let route = `/virtualvectors/${virtualVector._id}/edit`;
        chai
          .request(server)
          .post(route)
          .send({
            authorAffiliation: "fooford2",
            authorEmail: "foo@bar.baz2",
            authorName: "foobar",
            authorOrcid: "http://orcid/foo",
            name: "meep",
            description: "bar baz quay",
            status: "fooford",
            targetOrganisms: ["beep", "bap"],
            sequence: "nice",
            temperature: 2,
            positiveSelection: ["foo", "bar", "baz"],
            negativeSelection: ["baz", "bar", "foo"],
            genotypeConditions: ["bar", "baz", "foo"],
            media: ["yaa", "hoo"],
            createdByJob: "derp",
            derivedFrom: ["team", "do"],
            originNode: "meep"
          })
          .end((err, res) => {
            if (err) {
              console.log(err);
            }
            res.should.have.status(200);
            res.body.should.be.a("object");
            res.body.should.have.property("message");
            res.body.should.have.property("data");
            res.body.data.should.be.a("object");
            res.body.data.should.have
              .property("authorAffiliation")
              .eql("fooford2");
            res.body.data.should.have
              .property("authorEmail")
              .eql("foo@bar.baz2");
            res.body.data.should.have.property("authorName").eql("foobar");
            res.body.data.should.have.property("name").eql("meep");
            res.body.data.should.have
              .property("description")
              .eql("bar baz quay");
            res.body.data.should.have.property("status").eql("fooford");
            res.body.data.should.have
              .property("targetOrganisms")
              .eql(["beep", "bap"]);
            res.body.data.should.have.property("sequence").eql("nice");
            res.body.data.should.have.property("temperature").eql(2);
            res.body.data.should.have
              .property("positiveSelection")
              .eql(["foo", "bar", "baz"]);
            res.body.data.should.have
              .property("negativeSelection")
              .eql(["baz", "bar", "foo"]);
            res.body.data.should.have
              .property("genotypeConditions")
              .eql(["bar", "baz", "foo"]);
            res.body.data.should.have.property("media").eql(["yaa", "hoo"]);
            res.body.data.should.have.property("createdByJob").eql("derp");
            res.body.data.should.have
              .property("derivedFrom")
              .eql(["team", "do"]);
            res.body.data.should.have.property("originNode").eql("meep");
            done();
          });
      });
    });
  });

  describe("/POST /virtualvectors/:recordId/remove", () => {
    it("it should DELETE virtualVector by id", done => {
      let virtualVector = new VirtualVector({
        authorAffiliation: "fooford2",
        authorEmail: "foo@bar.baz2",
        authorName: "foobar",
        authorOrcid: "http://orcid/foo",
        name: "meep",
        description: "bar baz quay",
        status: "fooford",
        targetOrganisms: ["beep", "bap"],
        sequence: "nice",
        temperature: 2,
        positiveSelection: ["foo", "bar", "baz"],
        negativeSelection: ["baz", "bar", "foo"],
        genotypeConditions: ["bar", "baz", "foo"],
        media: ["yaa", "hoo"],
        createdByJob: "derp",
        derivedFrom: ["team", "do"],
        originNode: "meep"
      });
      virtualVector.save((error, virtualVector) => {
        let route = `/virtualvectors/${virtualVector._id}/remove`;
        chai
          .request(server)
          .post(route)
          .end((err, res) => {
            if (err) {
              console.log(err);
            }
            res.should.have.status(200);
            res.body.should.be.a("object");
            res.body.should.have
              .property("message")
              .eql("The record was successfully removed.");
            done();
          });
      });
    });
  });
});
