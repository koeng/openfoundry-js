//During the test the env variable is set to test
process.env.NODE_ENV = "test";

const mongoose = require("mongoose");
const shortid = require("shortid");
const VirtualOrganism = require("../../../models/Wetware/Virtuals/VirtualOrganism");

//Require the dev-dependencies
const chai = require("chai");
const chaiHttp = require("chai-http");
const server = require("../../../server");
const should = chai.should();

chai.use(chaiHttp);

describe("VirtualOrganism", () => {
  beforeEach(done => {
    VirtualOrganism.remove({}, err => {
      done();
    });
  });

  describe("/GET /virtualorganisms", () => {
    it("it should GET all the virtual organisms", done => {
      chai
        .request(server)
        .get("/virtualorganisms")
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.a("object");
          res.body.should.have.property("message");
          res.body.should.have.property("data");
          res.body.message.should.be.a("string");
          res.body.data.should.be.a("array");
          res.body.data.length.should.be.eql(0);
          done();
        });
    });
  });

  describe("/POST /virtualorganisms/new", () => {
    // author
    it("it should not POST a virtual organism without author field", done => {
      let virtualOrganism = {
        name: "baz",
        description: "bar baz quay",
        status: "fooford",
        authorEmail: "foo@bar.baz2",
        authorAffiliation: "fooford2",
        authorName: "foobar",
        authorOrcid: "http://orcid/foo",
        taxid: "barganism",
        dat_id: "baz",
        temperature: "fooooooooo",
        positiveSelection: ["foo", "bar", "baz"],
        negativeSelection: ["baz", "bar", "foo"],
        genotypeConditions: ["bar", "baz", "foo"],
        datName: "fooDat",
        datHash: "fooHash"
      };
      chai
        .request(server)
        .post("/virtualorganisms/new")
        .send(virtualOrganism)
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.a("object");
          res.body.should.have.property("error");
          res.body.error.should.have.property("errors");
          res.body.error.errors.should.have.property("author");
          res.body.error.errors.author.should.have
            .property("kind")
            .eql("required");
          done();
        });
    });
    // name
    it("it should not POST a virtual organism without name field", done => {
      let virtualOrganism = {
        author: "foos",
        description: "bar baz quay",
        status: "fooford",
        authorAffiliation: "fooford2",
        authorEmail: "foo@bar.baz2",
        authorName: "foobar",
        authorOrcid: "http://orcid/foo",
        taxid: "barganism",
        dat_id: "baz",
        temperature: "fooooooooo",
        positiveSelection: ["foo", "bar", "baz"],
        negativeSelection: ["baz", "bar", "foo"],
        genotypeConditions: ["bar", "baz", "foo"],
        datName: "fooDat",
        datHash: "fooHash"
      };
      chai
        .request(server)
        .post("/virtualorganisms/new")
        .send(virtualOrganism)
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.a("object");
          res.body.should.have.property("error");
          res.body.error.should.have.property("errors");
          res.body.error.errors.should.have.property("name");
          res.body.error.errors.name.should.have
            .property("kind")
            .eql("required");
          done();
        });
    });
    // authorEmail
    it("it should not POST a virtual organism without author email field", done => {
      let virtualOrganism = {
        author: "fooBar",
        name: "baz",
        description: "bar baz quay",
        status: "fooford",
        authorAffiliation: "foo bar",
        authorName: "foobar",
        authorOrcid: "http://orcid/foo",
        taxid: "barganism",
        dat_id: "baz",
        temperature: "fooooooooo",
        positiveSelection: ["foo", "bar", "baz"],
        negativeSelection: ["baz", "bar", "foo"],
        genotypeConditions: ["bar", "baz", "foo"],
        datName: "fooDat",
        datHash: "fooHash"
      };
      chai
        .request(server)
        .post("/virtualorganisms/new")
        .send(virtualOrganism)
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.a("object");
          res.body.should.have.property("error");
          res.body.error.should.have.property("errors");
          res.body.error.errors.should.have.property("authorEmail");
          res.body.error.errors.authorEmail.should.have
            .property("kind")
            .eql("required");
          done();
        });
      // authorName
    });
    it("it should not POST a virtual organism without author name field", done => {
      let virtualOrganism = {
        author: "fooBar",
        name: "baz",
        description: "bar baz quay",
        status: "fooford",
        authorAffiliation: "fooford2",
        authorEmail: "foo@bar.baz2",
        //authorName: "foobar",
        authorOrcid: "http://orcid/foo",
        taxid: "barganism",
        dat_id: "baz",
        temperature: "fooooooooo",
        positiveSelection: ["foo", "bar", "baz"],
        negativeSelection: ["baz", "bar", "foo"],
        genotypeConditions: ["bar", "baz", "foo"],
        datName: "fooDat",
        datHash: "fooHash"
      };
      chai
        .request(server)
        .post("/virtualorganisms/new")
        .send(virtualOrganism)
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.a("object");
          res.body.should.have.property("error");
          res.body.error.should.have.property("errors");
          res.body.error.errors.should.have.property("authorName");
          res.body.error.errors.authorName.should.have
            .property("kind")
            .eql("required");
          done();
        });
      // partType
      it("it should not POST a virtual organism without partType field", done => {
        let virtualOrganism = {
          author: "foob",
          name: "baz",
          description: "bar baz quay",
          status: "fooford",
          authorAffiliation: "fooford2",
          authorEmail: "foo@bar.baz2",
          authorName: "foobar",
          authorOrcid: "http://orcid/foo",
          taxid: "barganism",
          dat_id: "baz",
          temperature: "fooooooooo",
          positiveSelection: ["foo", "bar", "baz"],
          negativeSelection: ["baz", "bar", "foo"],
          genotypeConditions: ["bar", "baz", "foo"],
          datName: "fooDat",
          datHash: "fooHash"
        };
        chai
          .request(server)
          .post("/virtualorganisms/new")
          .send(virtualOrganism)
          .end((err, res) => {
            res.should.have.status(200);
            res.body.should.be.a("object");
            res.body.should.have.property("error");
            res.body.error.should.have.property("errors");
            res.body.error.errors.should.have.property("partType");
            res.body.error.errors.partType.should.have
              .property("kind")
              .eql("required");
            done();
          });
        // sequence
      });
      it("it should not POST a virtual organism without sequence field", done => {
        let virtualOrganism = {
          author: "foob",
          name: "baz",
          description: "bar baz quay",
          status: "fooford",
          authorAffiliation: "fooford2",
          authorEmail: "foo@bar.baz2",
          authorName: "foobar",
          authorOrcid: "http://orcid/foo",
          taxid: "barganism",
          dat_id: "baz",
          temperature: 8,
          positiveSelection: ["foo", "bar", "baz"],
          negativeSelection: ["baz", "bar", "foo"],
          genotypeConditions: ["bar", "baz", "foo"],
          datName: "fooDat",
          datHash: "fooHash"
        };
        chai
          .request(server)
          .post("/virtualorganisms/new")
          .send(virtualOrganism)
          .end((err, res) => {
            res.should.have.status(200);
            res.body.should.be.a("object");
            res.body.should.have.property("error");
            res.body.error.should.have.property("errors");
          });
      });
    });
    it("it should successfully POST a virtual organism", done => {
      let virtualOrganism = {
        author: "foobaz",
        name: "baz",
        description: "bar baz quay",
        status: "fooford",
        authorAffiliation: "fooford2",
        authorEmail: "foo@bar.baz2",
        authorName: "foobar",
        authorOrcid: "http://orcid/foo",
        taxid: "barganism",
        dat_id: "baz",
        temperature: 4,
        positiveSelection: ["foo", "bar", "baz"],
        negativeSelection: ["baz", "bar", "foo"],
        genotypeConditions: ["bar", "baz", "foo"],
        tags: ["foo", "bar", "beer"],
        datName: "fooDat",
        datHash: "fooHash"
      };
      chai
        .request(server)
        .post("/virtualorganisms/new")
        .send(virtualOrganism)
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.a("object");
          res.body.should.have.property("message");
          res.body.should.have.property("data");
          res.body.data.should.be.a("object");
          res.body.data.should.have.property("author");
          res.body.data.should.have.property("name");
          res.body.data.should.have.property("description");
          res.body.data.should.have.property("authorEmail");
          res.body.data.should.have.property("authorName");
          res.body.data.should.have.property("datName");
          res.body.data.should.have.property("datHash");
          res.body.data.should.have.property("tags");
          res.body.data.should.have.property("authorOrcid");
          res.body.data.should.have.property("authorAffiliation");
          res.body.data.should.have.property("taxid");
          res.body.data.should.have.property("positiveSelection");
          res.body.data.should.have.property("negativeSelection");
          res.body.data.should.have.property("genotypeConditions");
          done();
        });
    });
  });

  describe("/GET /virtualorganisms/:recordId", () => {
    it("it should GET a virtual organism by id", done => {
      let virtualOrganism = new VirtualOrganism({
        author: "foobaz",
        name: "baz",
        description: "bar baz quay",
        status: "fooford",
        authorAffiliation: "fooford2",
        authorEmail: "foo@bar.baz2",
        authorName: "foobar",
        authorOrcid: "http://orcid/foo",
        taxid: "barganism",
        dat_id: "baz",
        temperature: 4,
        positiveSelection: ["foo", "bar", "baz"],
        negativeSelection: ["baz", "bar", "foo"],
        genotypeConditions: ["bar", "baz", "foo"],
        tags: ["foo", "bar", "beer"],
        datName: "fooDat",
        datHash: "fooHash"
      });
      virtualOrganism.save((error, virtualOrganism) => {
        if (error) {
          console.log(error);
          done();
        } else {
          let route = `/virtualorganisms/${virtualOrganism._id}`;
          chai
            .request(server)
            .get(route)
            .send(virtualOrganism)
            .end((err, res) => {
              res.should.have.status(200);
              res.body.should.be.a("object");
              res.body.should.have.property("message");
              res.body.should.have.property("data");
              res.body.data.should.be.a("object");
              res.body.data.should.have.property("author");
              res.body.data.should.have.property("name");
              res.body.data.should.have.property("description");
              res.body.data.should.have.property("authorEmail");
              res.body.data.should.have.property("authorName");
              res.body.data.should.have.property("datName");
              res.body.data.should.have.property("datHash");
              res.body.data.should.have.property("tags");
              res.body.data.should.have.property("authorOrcid");
              res.body.data.should.have.property("authorAffiliation");
              done();
            });
        }
      });
    });
  });

  describe("/POST /virtualorganisms/:recordId/edit", () => {
    it("it should UPDATE virtual organism by id", done => {
      let virtualOrganism = new VirtualOrganism({
        author: "foobaz",
        name: "baz",
        description: "bar baz quay",
        status: "fooford",
        authorAffiliation: "fooford2",
        authorEmail: "foo@bar.baz2",
        authorName: "foobar",
        authorOrcid: "http://orcid/foo",
        taxid: "barganism",
        dat_id: "baz",
        temperature: 4,
        positiveSelection: ["foo", "bar", "baz"],
        negativeSelection: ["baz", "bar", "foo"],
        genotypeConditions: ["bar", "baz", "foo"],
        datName: "fooDat",
        datHash: "fooHash",
        tags: ["foo", "bar", "beer"]
      });
      virtualOrganism.save((error, virtualOrganism) => {
        if (error) {
          console.log(error);
        }
        let route = `/virtualorganisms/${virtualOrganism._id}/edit`;
        chai
          .request(server)
          .post(route)
          .send({
            author: "foo2",
            name: "baz2",
            description: "bar baz quay",
            status: "fooford",
            authorAffiliation: "fooford2",
            authorEmail: "foo@bar.baz2",
            authorName: "foobar",
            authorOrcid: "http://orcid/foo",
            taxid: "barganism",
            dat_id: "baz",
            temperature: 4,
            positiveSelection: ["foo", "bar", "baz"],
            negativeSelection: ["baz", "bar", "foo"],
            genotypeConditions: ["bar", "baz", "foo"],
            tags: ["foo", "bar", "beer"],
            datName: "fooDat",
            datHash: "fooHash"
          })
          .end((err, res) => {
            if (err) {
              console.log(err);
            }
            res.should.have.status(200);
            res.body.should.be.a("object");
            res.body.should.have.property("message");
            res.body.should.have.property("data");
            res.body.data.should.be.a("object");
            res.body.data.should.have.property("author").eql("foo2");
            res.body.data.should.have.property("name").eql("baz2");
            res.body.data.should.have
              .property("description")
              .eql("bar baz quay");
            res.body.data.should.have
              .property("authorAffiliation")
              .eql("fooford2");
            res.body.data.should.have
              .property("authorEmail")
              .eql("foo@bar.baz2");
            res.body.data.should.have.property("authorName").eql("foobar");
            res.body.data.should.have
              .property("authorOrcid")
              .eql("http://orcid/foo");
            res.body.data.should.have
              .property("tags")
              .eql(["foo", "bar", "beer"]);
            res.body.data.should.have.property("datName").eql("fooDat");
            res.body.data.should.have.property("datHash").eql("fooHash");
            done();
          });
      });
    });
  });

  describe("/POST /virtualorganisms/:recordId/remove", () => {
    it("it should DELETE virtual organism by id", done => {
      let virtualOrganism = new VirtualOrganism({
        author: "foobaz",
        name: "baz",
        description: "bar baz quay",
        status: "fooford",
        authorAffiliation: "fooford2",
        authorEmail: "foo@bar.baz2",
        authorName: "foobar",
        authorOrcid: "http://orcid/foo",
        taxid: "barganism",
        dat_id: "baz",
        temperature: 4,
        positiveSelection: ["foo", "bar", "baz"],
        negativeSelection: ["baz", "bar", "foo"],
        genotypeConditions: ["bar", "baz", "foo"],
        datName: "fooDat",
        datHash: "fooHash"
      });
      virtualOrganism.save((error, virtualOrganism) => {
        let route = `/virtualorganisms/${virtualOrganism._id}/remove`;
        chai
          .request(server)
          .post(route)
          .end((err, res) => {
            if (err) {
              console.log(err);
            }
            res.should.have.status(200);
            res.body.should.be.a("object");
            res.body.should.have
              .property("message")
              .eql("The record was successfully removed.");
            done();
          });
      });
    });
  });
});
