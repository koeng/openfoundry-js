//During the test the env variable is set to test
process.env.NODE_ENV = "test";

const mongoose = require("mongoose");
const shortid = require("shortid");
const VirtualPart = require("../../../models/Wetware/Virtuals/VirtualPart");

//Require the dev-dependencies
const chai = require("chai");
const chaiHttp = require("chai-http");
const server = require("../../../server");
const should = chai.should();

chai.use(chaiHttp);

describe("VirtualPart", () => {
  beforeEach(done => {
    VirtualPart.remove({}, err => {
      done();
    });
  });

  describe("/GET /virtualparts", () => {
    it("it should GET all the virtualparts", done => {
      chai
        .request(server)
        .get("/virtualparts")
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.a("object");
          res.body.should.have.property("message");
          res.body.should.have.property("data");
          res.body.message.should.be.a("string");
          res.body.data.should.be.a("array");
          res.body.data.length.should.be.eql(0);
          done();
        });
    });
  });

  describe("/POST /virtualparts/new", () => {
    // author
    it("it should not POST a virtualpart without author field", done => {
      let virtualpart = {
        name: "baz",
        description: "bar baz quay",
        authorAffiliation: "fooford",
        authorEmail: "foo@bar.baz",
        authorName: "foo baz",
        authorOrcid: "http://orcid/foo",
        targetOrganism: "foOrganism",
        sourceOrganism: "barganism",
        partType: "baz",
        sequence: "fooooooooo",
        tags: ["foo", "bar", "baz"],
        derivedFrom: ["baz", "bar", "foo"],
        datName: "fooDat",
        datHash: "fooHash"
      };
      chai
        .request(server)
        .post("/virtualparts/new")
        .send(virtualpart)
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.a("object");
          res.body.should.have.property("error");
          res.body.error.should.have.property("errors");
          res.body.error.errors.should.have.property("author");
          res.body.error.errors.author.should.have
            .property("kind")
            .eql("required");
          done();
        });
    });
    // name
    it("it should not POST a virtualpart without name field", done => {
      let virtualpart = {
        author: "foo",
        description: "bar baz quay",
        authorAffiliation: "fooford",
        authorEmail: "foo@bar.baz",
        authorName: "foo baz",
        authorOrcid: "http://orcid/foo",
        targetOrganism: "foOrganism",
        sourceOrganism: "barganism",
        partType: "baz",
        sequence: "fooooooooo",
        tags: ["foo", "bar", "baz"],
        derivedFrom: ["baz", "bar", "foo"],
        datName: "fooDat",
        datHash: "fooHash"
      };
      chai
        .request(server)
        .post("/virtualparts/new")
        .send(virtualpart)
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.a("object");
          res.body.should.have.property("error");
          res.body.error.should.have.property("errors");
          res.body.error.errors.should.have.property("name");
          res.body.error.errors.name.should.have
            .property("kind")
            .eql("required");
          done();
        });
    });
    // authorEmail
    it("it should not POST a virtualpart without author email field", done => {
      let virtualpart = {
        name: "baz",
        author: "foo",
        description: "bar baz quay",
        authorAffiliation: "fooford",
        authorName: "foo baz",
        authorOrcid: "http://orcid/foo",
        targetOrganism: "foOrganism",
        sourceOrganism: "barganism",
        partType: "baz",
        sequence: "fooooooooo",
        tags: ["foo", "bar", "baz"],
        derivedFrom: ["baz", "bar", "foo"],
        datName: "fooDat",
        datHash: "fooHash"
      };
      chai
        .request(server)
        .post("/virtualparts/new")
        .send(virtualpart)
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.a("object");
          res.body.should.have.property("error");
          res.body.error.should.have.property("errors");
          res.body.error.errors.should.have.property("authorEmail");
          res.body.error.errors.authorEmail.should.have
            .property("kind")
            .eql("required");
          done();
        });
      // authorName
    });
    it("it should not POST a virtualpart without author name field", done => {
      let virtualpart = {
        name: "baz",
        author: "foo",
        description: "bar baz quay",
        authorAffiliation: "fooford",
        authorEmail: "foo@bar.baz",
        authorOrcid: "http://orcid/foo",
        targetOrganism: "foOrganism",
        sourceOrganism: "barganism",
        partType: "baz",
        sequence: "fooooooooo",
        tags: ["foo", "bar", "baz"],
        derivedFrom: ["baz", "bar", "foo"],
        datName: "fooDat",
        datHash: "fooHash"
      };
      chai
        .request(server)
        .post("/virtualparts/new")
        .send(virtualpart)
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.a("object");
          res.body.should.have.property("error");
          res.body.error.should.have.property("errors");
          res.body.error.errors.should.have.property("authorName");
          res.body.error.errors.authorName.should.have
            .property("kind")
            .eql("required");
          done();
        });
      // partType
      it("it should not POST a virtualpart without partType field", done => {
        let virtualpart = {
          name: "baz",
          author: "foo",
          description: "bar baz quay",
          authorAffiliation: "fooford",
          authorEmail: "foo@bar.baz",
          authorName: "foo baz",
          authorOrcid: "http://orcid/foo",
          targetOrganism: "foOrganism",
          sourceOrganism: "barganism",
          sequence: "fooooooooo",
          tags: ["foo", "bar", "baz"],
          derivedFrom: ["baz", "bar", "foo"],
          datName: "fooDat",
          datHash: "fooHash"
        };
        chai
          .request(server)
          .post("/virtualpartparts/new")
          .send(virtualpart)
          .end((err, res) => {
            res.should.have.status(200);
            res.body.should.be.a("object");
            res.body.should.have.property("error");
            res.body.error.should.have.property("errors");
            res.body.error.errors.should.have.property("partType");
            res.body.error.errors.partType.should.have
              .property("kind")
              .eql("required");
            done();
          });
        // sequence
      });
      it("it should not POST a virtualpart without sequence field", done => {
        let virtualpart = {
          name: "baz",
          author: "foo",
          description: "bar baz quay",
          authorAffiliation: "fooford",
          authorEmail: "foo@bar.baz",
          authorName: "foo baz",
          authorOrcid: "http://orcid/foo",
          targetOrganism: "foOrganism",
          sourceOrganism: "barganism",
          partType: "baz",
          tags: ["foo", "bar", "baz"],
          derivedFrom: ["baz", "bar", "foo"],
          datName: "fooDat",
          datHash: "fooHash"
        };
        chai
          .request(server)
          .post("/virtualparts/new")
          .send(virtualpart)
          .end((err, res) => {
            res.should.have.status(200);
            res.body.should.be.a("object");
            res.body.should.have.property("error");
            res.body.error.should.have.property("errors");
            res.body.error.errors.should.have.property("sequence");
            res.body.error.errors.sequence.should.have
              .property("kind")
              .eql("required");
            done();
          });
      });
    });
    it("it should successfully POST a virtualpart", done => {
      let virtualpart = {
        name: "baz",
        author: "foo",
        description: "bar baz quay",
        authorAffiliation: "fooford",
        authorEmail: "foo@bar.baz",
        authorName: "foo baz",
        authorOrcid: "http://orcid/foo",
        targetOrganism: "foOrganism",
        sourceOrganism: "barganism",
        partType: "baz",
        sequence: "fooooooooo",
        tags: ["foo", "bar", "baz"],
        derivedFrom: ["baz", "bar", "foo"],
        datName: "fooDat",
        datHash: "fooHash"
      };
      chai
        .request(server)
        .post("/virtualparts/new")
        .send(virtualpart)
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.a("object");
          res.body.should.have.property("message");
          res.body.should.have.property("data");
          res.body.data.should.be.a("object");
          res.body.data.should.have.property("author");
          res.body.data.should.have.property("name");
          res.body.data.should.have.property("description");
          res.body.data.should.have.property("authorEmail");
          res.body.data.should.have.property("authorName");
          res.body.data.should.have.property("sequence");
          res.body.data.should.have.property("partType");
          res.body.data.should.have.property("datName");
          res.body.data.should.have.property("datHash");
          res.body.data.should.have.property("targetOrganism");
          res.body.data.should.have.property("sourceOrganism");
          res.body.data.should.have.property("tags");
          res.body.data.should.have.property("derivedFrom");
          res.body.data.should.have.property("authorOrcid");
          res.body.data.should.have.property("authorAffiliation");
          done();
        });
    });
  });

  describe("/GET /virtualparts/:recordId", () => {
    it("it should GET a virtualpart by id", done => {
      let virtualpart = new VirtualPart({
        name: "baz",
        author: "foo",
        description: "bar baz quay",
        authorAffiliation: "fooford",
        authorEmail: "foo@bar.baz",
        authorName: "foo baz",
        authorOrcid: "http://orcid/foo",
        targetOrganism: "foOrganism",
        sourceOrganism: "barganism",
        partType: "baz",
        sequence: "fooooooooo",
        tags: ["foo", "bar", "baz"],
        derivedFrom: ["baz", "bar", "foo"],
        datName: "fooDat",
        datHash: "fooHash"
      });
      virtualpart.save((error, virtualpart) => {
        if (error) {
          console.log(error);
          done();
        } else {
          let route = `/virtualparts/${virtualpart._id}`;
          chai
            .request(server)
            .get(route)
            .send(virtualpart)
            .end((err, res) => {
              res.should.have.status(200);
              res.body.should.be.a("object");
              res.body.should.have.property("message");
              res.body.should.have.property("data");
              res.body.data.should.be.a("object");
              res.body.data.should.have.property("author");
              res.body.data.should.have.property("name");
              res.body.data.should.have.property("description");
              res.body.data.should.have.property("authorEmail");
              res.body.data.should.have.property("authorName");
              res.body.data.should.have.property("sequence");
              res.body.data.should.have.property("partType");
              res.body.data.should.have.property("datName");
              res.body.data.should.have.property("datHash");
              res.body.data.should.have.property("targetOrganism");
              res.body.data.should.have.property("sourceOrganism");
              res.body.data.should.have.property("tags");
              res.body.data.should.have.property("derivedFrom");
              res.body.data.should.have.property("authorOrcid");
              res.body.data.should.have.property("authorAffiliation");
              done();
            });
        }
      });
    });
  });

  describe("/POST /virtualparts/:recordId/edit", () => {
    it("it should UPDATE virtualpart by id", done => {
      let virtualpart = new VirtualPart({
        author: "foo",
        name: "baz",
        description: "bar baz quay",
        authorAffiliation: "fooford",
        authorEmail: "foo@bar.baz",
        authorName: "foo baz",
        authorOrcid: "http://orcid/foo",
        targetOrganism: "foOrganism",
        sourceOrganism: "barganism",
        partType: "baz",
        sequence: "fooooooooo",
        tags: ["foo", "bar", "baz"],
        derivedFrom: ["baz", "bar", "foo"],
        datName: "fooDat",
        datHash: "fooHash"
      });
      virtualpart.save((error, virtualpart) => {
        if (error) {
          console.log(error);
        }
        let route = `/virtualparts/${virtualpart._id}/edit`;
        chai
          .request(server)
          .post(route)
          .send({
            author: "foo2",
            name: "baz2",
            description: "bar baz quay2",
            authorAffiliation: "fooford2",
            authorEmail: "foo@bar.baz2",
            authorName: "foo baz2",
            authorOrcid: "http://orcid/foo2",
            targetOrganism: "foOrganism2",
            sourceOrganism: "barganism2",
            partType: "baz2",
            sequence: "fooooooooo2",
            tags: ["foo2", "bar", "baz"],
            derivedFrom: ["baz", "bar", "foo2"],
            datName: "fooDat2",
            datHash: "fooHash2"
          })
          .end((err, res) => {
            if (err) {
              console.log(err);
            }
            res.should.have.status(200);
            res.body.should.be.a("object");
            res.body.should.have.property("message");
            res.body.should.have.property("data");
            res.body.data.should.be.a("object");
            res.body.data.should.have.property("author").eql("foo2");
            res.body.data.should.have.property("name").eql("baz2");
            res.body.data.should.have
              .property("description")
              .eql("bar baz quay2");
            res.body.data.should.have
              .property("authorAffiliation")
              .eql("fooford2");
            res.body.data.should.have
              .property("authorEmail")
              .eql("foo@bar.baz2");
            res.body.data.should.have
              .property("targetOrganism")
              .eql("foOrganism2");
            res.body.data.should.have
              .property("sourceOrganism")
              .eql("barganism2");
            res.body.data.should.have.property("authorName").eql("foo baz2");
            res.body.data.should.have.property("partType").eql("baz2");
            res.body.data.should.have
              .property("authorOrcid")
              .eql("http://orcid/foo2");
            res.body.data.should.have
              .property("derivedFrom")
              .eql(["baz", "bar", "foo2"]);
            res.body.data.should.have.property("sequence").eql("fooooooooo2");
            res.body.data.should.have
              .property("tags")
              .eql(["foo2", "bar", "baz"]);
            res.body.data.should.have.property("datName").eql("fooDat2");
            res.body.data.should.have.property("datHash").eql("fooHash2");
            done();
          });
      });
    });
  });

  describe("/POST /virtualparts/:recordId/remove", () => {
    it("it should DELETE virtualpart by id", done => {
      let virtualpart = new VirtualPart({
        name: "baz",
        author: "foo",
        description: "bar baz quay",
        authorAffiliation: "fooford",
        authorEmail: "foo@bar.baz",
        authorName: "foo baz",
        authorOrcid: "http://orcid/foo",
        targetOrganism: "foOrganism",
        sourceOrganism: "barganism",
        partType: "baz",
        sequence: "fooooooooo",
        tags: ["foo", "bar", "baz"],
        derivedFrom: ["baz", "bar", "foo"],
        datName: "fooDat",
        datHash: "fooHash"
      });
      virtualpart.save((error, virtualpart) => {
        let route = `/virtualparts/${virtualpart._id}/remove`;
        chai
          .request(server)
          .post(route)
          .end((err, res) => {
            if (err) {
              console.log(err);
            }
            res.should.have.status(200);
            res.body.should.be.a("object");
            res.body.should.have
              .property("message")
              .eql("The record was successfully removed.");
            done();
          });
      });
    });
  });
});
