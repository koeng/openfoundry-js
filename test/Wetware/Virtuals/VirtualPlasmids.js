//During the test the env variable is set to test
process.env.NODE_ENV = "test";

const mongoose = require("mongoose");
const shortid = require("shortid");
const VirtualPlasmid = require("../../../models/Wetware/Virtuals/VirtualPlasmid");

//Require the dev-dependencies
const chai = require("chai");
const chaiHttp = require("chai-http");
const server = require("../../../server");
const should = chai.should();

chai.use(chaiHttp);

describe("Virtual Plasmid", () => {
  beforeEach(done => {
    VirtualPlasmid.remove({}, err => {
      done();
    });
  });

  describe("/GET /virtualplasmids", () => {
    it("it should GET all the virtual plasmids", done => {
      chai
        .request(server)
        .get("/virtualplasmids")
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.a("object");
          res.body.should.have.property("message");
          res.body.should.have.property("data");
          res.body.message.should.be.a("string");
          res.body.data.should.be.a("array");
          res.body.data.length.should.be.eql(0);
          done();
        });
    });
  });

  describe("/POST /virtualplasmids/new", () => {
    // author
    it("it should not POST a virtual plasmid without author field", done => {
      let virtualPlasmid = {
        name: "bur",
        description: "bar baz quay",
        status: "fooford",
        authorAffiliation: "fooford2",
        authorEmail: "foo@bar.baz2",
        authorName: "foobar",
        authorOrcid: "http://orcid/foo",
        sequence: "beep",
        cloningMethod: "boop",
        tags: ["clip", "clop"],
        functionsIn: ["boop()", "beep()"],
        part: "yee",
        vector: "bop",
        datName: "fooDat",
        datHash: "fooHash",
        derivedFrom: ["bop", "bap"]
      };
      chai
        .request(server)
        .post("/virtualplasmids/new")
        .send(virtualPlasmid)
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.a("object");
          res.body.should.have.property("error");
          res.body.error.should.have.property("errors");
          res.body.error.errors.should.have.property("author");
          res.body.error.errors.author.should.have
            .property("kind")
            .eql("required");
          done();
        });
    });
    // name
    it("it should not POST a virtual plasmid without name field", done => {
      let virtualPlasmid = {
        author: "foo",
        description: "bar baz quay",
        status: "fooford",
        authorAffiliation: "fooford2",
        authorEmail: "foo@bar.baz2",
        authorName: "foobar",
        authorOrcid: "http://orcid/foo",
        sequence: "beep",
        cloningMethod: "boop",
        tags: ["clip", "clop"],
        functionsIn: ["boop()", "beep()"],
        part: "yee",
        vector: "bop",
        datName: "fooDat",
        datHash: "fooHash",
        derivedFrom: ["bop", "bap"]
      };
      chai
        .request(server)
        .post("/virtualplasmids/new")
        .send(virtualPlasmid)
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.a("object");
          res.body.should.have.property("error");
          res.body.error.should.have.property("errors");
          res.body.error.errors.should.have.property("name");
          res.body.error.errors.name.should.have
            .property("kind")
            .eql("required");
          done();
        });
    });
    // authorEmail
    it("it should not POST a virtual plasmid without author email field", done => {
      let virtualPlasmid = {
        author: "foo",
        name: "bur",
        description: "bar baz quay",
        status: "fooford",
        authorAffiliation: "fooford2",
        authorName: "foobar",
        authorOrcid: "http://orcid/foo",
        sequence: "beep",
        cloningMethod: "boop",
        tags: ["clip", "clop"],
        functionsIn: ["boop()", "beep()"],
        part: "yee",
        vector: "bop",
        datName: "fooDat",
        datHash: "fooHash",
        derivedFrom: ["bop", "bap"]
      };
      chai
        .request(server)
        .post("/virtualplasmids/new")
        .send(virtualPlasmid)
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.a("object");
          res.body.should.have.property("error");
          res.body.error.should.have.property("errors");
          res.body.error.errors.should.have.property("authorEmail");
          res.body.error.errors.authorEmail.should.have
            .property("kind")
            .eql("required");
          done();
        });
      // authorName
    });
    it("it should not POST a virtual plasmid without author name field", done => {
      let virtualPlasmid = {
        author: "foo",
        name: "bur",
        description: "bar baz quay",
        status: "fooford",
        authorAffiliation: "fooford2",
        authorEmail: "foo@bar.baz2",
        authorOrcid: "http://orcid/foo",
        sequence: "beep",
        cloningMethod: "boop",
        tags: ["clip", "clop"],
        functionsIn: ["boop()", "beep()"],
        part: "yee",
        vector: "bop",
        datName: "fooDat",
        datHash: "fooHash",
        derivedFrom: ["bop", "bap"]
      };
      chai
        .request(server)
        .post("/virtualplasmids/new")
        .send(virtualPlasmid)
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.a("object");
          res.body.should.have.property("error");
          res.body.error.should.have.property("errors");
          res.body.error.errors.should.have.property("authorName");
          res.body.error.errors.authorName.should.have
            .property("kind")
            .eql("required");
          done();
        });
    });

    // sequence
    it("it should not POST a virtual plasmid without sequence field", done => {
      let virtualPlasmid = {
        author: "foo",
        name: "bur",
        description: "bar baz quay",
        status: "fooford",
        authorAffiliation: "fooford2",
        authorEmail: "foo@bar.baz2",
        authorName: "foobar",
        authorOrcid: "http://orcid/foo",
        cloningMethod: "boop",
        tags: ["clip", "clop"],
        functionsIn: ["boop()", "beep()"],
        part: "yee",
        vector: "bop",
        datName: "fooDat",
        datHash: "fooHash",
        derivedFrom: ["bop", "bap"]
      };
      chai
        .request(server)
        .post("/virtualplasmids/new")
        .send(virtualPlasmid)
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.a("object");
          res.body.should.have.property("error");
          res.body.error.should.have.property("errors");
          res.body.error.errors.should.have.property("sequence");
          res.body.error.errors.sequence.should.have
            .property("kind")
            .eql("required");
          done();
        });
    });
    it("it should successfully POST a virtual plasmid", done => {
      let virtualPlasmid = {
        author: "foo",
        name: "bur",
        description: "bar baz quay",
        status: "fooford",
        authorAffiliation: "fooford2",
        authorEmail: "foo@bar.baz2",
        authorName: "foobar",
        authorOrcid: "http://orcid/foo",
        sequence: "beep",
        cloningMethod: "boop",
        tags: ["clip", "clop"],
        functionsIn: ["boop()", "beep()"],
        part: "yee",
        vector: "bop",
        datName: "fooDat",
        datHash: "fooHash",
        derivedFrom: ["bop", "bap"]
      };
      chai
        .request(server)
        .post("/virtualplasmids/new")
        .send(virtualPlasmid)
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.a("object");
          res.body.should.have.property("message");
          res.body.should.have.property("data");
          res.body.data.should.be.a("object");
          res.body.data.should.have.property("author");
          res.body.data.should.have.property("name");
          res.body.data.should.have.property("description");
          res.body.data.should.have.property("authorAffiliation");
          res.body.data.should.have.property("authorEmail");
          res.body.data.should.have.property("authorName");
          res.body.data.should.have.property("authorOrcid");
          res.body.data.should.have.property("sequence");
          res.body.data.should.have.property("cloningMethod");
          res.body.data.should.have.property("tags");
          res.body.data.should.have.property("functionsIn");
          res.body.data.should.have.property("part");
          res.body.data.should.have.property("vector");
          res.body.data.should.have.property("datName");
          res.body.data.should.have.property("datHash");
          res.body.data.should.have.property("derivedFrom");
          done();
        });
    });
  }); // end of post new

  describe("/GET /virtualplasmids/:recordId", () => {
    it("it should GET a virtual plasmid by id", done => {
      let virtualPlasmid = new VirtualPlasmid({
        author: "foo",
        name: "bur",
        description: "bar baz quay",
        status: "fooford",
        authorAffiliation: "fooford2",
        authorEmail: "foo@bar.baz2",
        authorName: "foobar",
        authorOrcid: "http://orcid/foo",
        sequence: "beep",
        cloningMethod: "boop",
        tags: ["clip", "clop"],
        functionsIn: ["boop()", "beep()"],
        part: "yee",
        vector: "bop",
        datName: "fooDat",
        datHash: "fooHash",
        derivedFrom: ["bop", "bap"]
      });
      virtualPlasmid.save((error, virtualPlasmid) => {
        if (error) {
          console.log(error);
          done();
        } else {
          let route = `/virtualplasmids/${virtualPlasmid._id}`;
          chai
            .request(server)
            .get(route)
            .send(virtualPlasmid)
            .end((err, res) => {
              res.should.have.status(200);
              res.body.should.be.a("object");
              res.body.should.have.property("message");
              res.body.should.have.property("data");
              res.body.data.should.be.a("object");
              res.body.data.should.have.property("author");
              res.body.data.should.have.property("name");
              res.body.data.should.have.property("description");
              res.body.data.should.have.property("authorEmail");
              res.body.data.should.have.property("authorName");
              res.body.data.should.have.property("datName");
              res.body.data.should.have.property("datHash");
              res.body.data.should.have.property("tags");
              res.body.data.should.have.property("authorOrcid");
              res.body.data.should.have.property("authorAffiliation");
              done();
            });
        }
      });
    });
  });

  describe("/POST /virtualplasmids/:recordId/edit", () => {
    it("it should UPDATE virtual plasmid by id", done => {
      let virtualPlasmid = new VirtualPlasmid({
        author: "foo",
        name: "bur",
        description: "bar baz quay",
        status: "fooford",
        authorAffiliation: "fooford2",
        authorEmail: "foo@bar.baz2",
        authorName: "foobar",
        authorOrcid: "http://orcid/foo",
        sequence: "beep",
        cloningMethod: "boop",
        tags: ["clip", "clop"],
        functionsIn: ["boop()", "beep()"],
        part: "yee",
        vector: "bop",
        datName: "fooDat",
        datHash: "fooHash",
        derivedFrom: ["bop", "bap"]
      });
      virtualPlasmid.save((error, virtualPlasmid) => {
        if (error) {
          console.log(error);
        }
        let route = `/virtualplasmids/${virtualPlasmid._id}/edit`;
        chai
          .request(server)
          .post(route)
          .send({
            author: "foo",
            name: "bur",
            description: "bar baz quay",
            status: "fooford",
            authorAffiliation: "fooford2",
            authorEmail: "foo@bar.baz2",
            authorName: "foobar",
            authorOrcid: "http://orcid/foo",
            sequence: "beep",
            cloningMethod: "boop",
            tags: ["clip", "clop"],
            functionsIn: ["boop()", "beep()"],
            part: "yee",
            vector: "bop",
            datName: "fooDat",
            datHash: "fooHash",
            derivedFrom: ["bop", "bap"]
          })
          .end((err, res) => {
            if (err) {
              console.log(err);
            }
            res.should.have.status(200);
            res.body.should.be.a("object");
            res.body.should.have.property("message");
            res.body.should.have.property("data");
            res.body.data.should.be.a("object");
            res.body.data.should.have.property("author").eql("foo");
            res.body.data.should.have.property("name").eql("bur");
            res.body.data.should.have.property("description").eql("bar baz quay");
            res.body.data.should.have
              .property("authorAffiliation")
              .eql("fooford2");
            res.body.data.should.have.property("authorEmail").eql("foo@bar.baz2");
            res.body.data.should.have.property("authorName").eql("foobar");
            res.body.data.should.have
              .property("authorOrcid")
              .eql("http://orcid/foo");
            res.body.data.should.have.property("tags").eql(["clip", "clop"]);
            res.body.data.should.have.property("datName").eql("fooDat");
            res.body.data.should.have.property("datHash").eql("fooHash");

            res.body.data.should.have
              .property("functionsIn")
              .eql(["boop()", "beep()"]);
            res.body.data.should.have.property("part").eql("yee");
            res.body.data.should.have.property("sequence").eql("beep");
            res.body.data.should.have.property("vector").eql("bop");
            res.body.data.should.have.property("cloningMethod").eql("boop");
            res.body.data.should.have.property("derivedFrom").eql(["bop", "bap"]);
            done();
          });
      });
    });
  });

  describe("/POST /virtualplasmids/:recordId/remove", () => {
    it("it should DELETE virtual plasmid by id", done => {
      let virtualPlasmid = new VirtualPlasmid({
        author: "foo",
        name: "bur",
        description: "bar baz quay",
        status: "fooford",
        authorAffiliation: "fooford2",
        authorEmail: "foo@bar.baz2",
        authorName: "foobar",
        authorOrcid: "http://orcid/foo",
        sequence: "beep",
        cloningMethod: "boop",
        tags: ["clip", "clop"],
        functionsIn: ["boop()", "beep()"],
        part: "yee",
        vector: "bop",
        datName: "fooDat",
        datHash: "fooHash",
        derivedFrom: ["bop", "bap"]
      });
      virtualPlasmid.save((error, virtualPlasmid) => {
        let route = `/virtualplasmids/${virtualPlasmid._id}/remove`;
        chai
          .request(server)
          .post(route)
          .end((err, res) => {
            if (err) {
              console.log(err);
            }
            res.should.have.status(200);
            res.body.should.be.a("object");
            res.body.should.have
              .property("message")
              .eql("The record was successfully removed.");
            done();
          });
      });
    });
  });

}); // end parent describe

