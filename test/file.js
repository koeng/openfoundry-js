//During the test the env variable is set to test
process.env.NODE_ENV = "test";

const mongoose = require("mongoose");
const shortid = require("shortid");
const File = require("../models/File");

//Require the dev-dependencies
const chai = require("chai");
const chaiHttp = require("chai-http");
const server = require("../server");
const should = chai.should();

chai.use(chaiHttp);

describe("Files", () => {
  beforeEach(done => {
    File.remove({}, err => {
      done();
    });
  });

  describe("/GET /files", () => {
    it("it should GET all the files", done => {
      chai
        .request(server)
        .get("/files")
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.a("object");
          res.body.should.have.property("message");
          res.body.should.have.property("data");
          res.body.message.should.be.a("string");
          res.body.data.should.be.a("array");
          res.body.data.length.should.be.eql(0);
          done();
        });
    });
  });

  describe("/POST /files/new", () => {
    it("it should not POST without file field filled", done => {
      let file = {
        fileName: "yee",
        fileType: "yuk",
        datName: "fooDat",
        datHash: "fooHash"
      };
      chai
        .request(server)
        .post("/files/new")
        .send(file)
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.a("object");
          res.body.should.have.property("error");
          res.body.error.should.have.property("errors");
          res.body.error.errors.should.have.property("file");
          res.body.error.errors.file.should.have
            .property("kind")
            .eql("required");
          done();
        });
    });
    it("it should not POST a file without fileName field", done => {
      let file = {
        file: "yahoo",
        fileType: "yuk",
        datName: "fooDat",
        datHash: "fooHash"
      };
      chai
        .request(server)
        .post("/files/new")
        .send(file)
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.a("object");
          res.body.should.have.property("error");
          res.body.error.should.have.property("errors");
          res.body.error.errors.should.have.property("fileName");
          res.body.error.errors.fileName.should.have
            .property("kind")
            .eql("required");
          done();
        });
    });
    it("it should not POST a file without file type field", done => {
      let file = {
        file: "yahoo",
        fileName: "yee",
        datName: "fooDat",
        datHash: "fooHash"
      };
      chai
        .request(server)
        .post("/files/new")
        .send(file)
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.a("object");
          res.body.should.have.property("error");
          res.body.error.should.have.property("errors");
          res.body.error.errors.should.have.property("fileType");
          res.body.error.errors.fileType.should.have
            .property("kind")
            .eql("required");
          done();
        });
    });
    it("it should successfully POST a file", done => {
      let file = {
        file: "yahoo",
        fileName: "yee",
        fileType: "yuk",
        datName: "fooDat",
        datHash: "fooHash"
      };
      chai
        .request(server)
        .post("/files/new")
        .send(file)
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.a("object");
          res.body.should.have.property("message");
          res.body.should.have.property("data");
          res.body.data.should.be.a("object");
          res.body.data.should.have.property("file");
          res.body.data.should.have.property("fileName");
          res.body.data.should.have.property("fileType");
          done();
        });
    });
  });

  describe("/GET /files/:recordId", () => {
    it("it should GET a file by id", done => {
      let file = new File({
        file: "yahoo",
        fileName: "yee",
        fileType: "yuk",
        datName: "fooDat",
        datHash: "fooHash"
      });
      file.save((error, file) => {
        if (error) {
          console.log(error);
          done();
        } else {
          let route = `/files/${file._id}`;
          chai
            .request(server)
            .get(route)
            .send(file)
            .end((err, res) => {
              if (err) {
                console.log(err);
              }
                res.should.have.status(200);
                res.body.should.be.a("object");
                res.body.should.have.property("message");
                res.body.should.have.property("data");
                res.body.data.should.be.a("object");
                res.body.data.should.have.property("file");
                res.body.data.should.have.property("fileName");
                res.body.data.should.have.property("fileType");
                res.body.data.should.have.property("datName");
                res.body.data.should.have.property("datHash");
              done();
            });
        }
      });
    });
  });

  describe("/POST /files/:recordId/edit", () => {
    it("it should UPDATE file by id", done => {
      let file = new File({
        file: "yahoo",
        fileName: "yee",
        fileType: "yuk",
        datName: "fooDat",
        datHash: "fooHash"
      });
      file.save((error, file) => {
        if (error) {
          console.log(error);
        }
        let route = `/files/${file._id}/edit`;
        chai
          .request(server)
          .post(route)
          .send({
            file: "yahoo",
            fileName: "yee",
            fileType: "yuk",
            datName: "fooDat",
            datHash: "fooHash"
          })
          .end((err, res) => {
            if (err) {
              console.log(err);
            }
            res.should.have.status(200);
            res.body.should.be.a("object");
            res.body.should.have.property("message");
            res.body.should.have.property("data");
            res.body.data.should.be.a("object");
            res.body.data.should.have.property("file").eql("yahoo");
            res.body.data.should.have.property("fileName").eql("yee");
            res.body.data.should.have.property("fileType").eql("yuk");
            res.body.data.should.have.property("datName").eql("fooDat");
            res.body.data.should.have.property("datHash").eql("fooHash");
            done();
          });
      });
    });
  });

  describe("/POST /files/:recordId/remove", () => {
    it("it should DELETE file by id", done => {
      let file = new File({
        file: "yahoo",
        fileName: "yee",
        fileType: "yuk",
        datName: "fooDat",
        datHash: "fooHash"
      });
      file.save((error, file) => {
        let route = `/files/${file._id}/remove`;
        chai
          .request(server)
          .post(route)
          .end((err, res) => {
            if (err) {
              console.log(err);
            }
            res.should.have.status(200);
            res.body.should.be.a("object");
            res.body.should.have
              .property("message")
              .eql("The record was successfully removed.");
            done();
          });
      });
    });
  });
});
